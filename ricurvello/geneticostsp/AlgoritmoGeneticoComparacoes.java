package ricurvello.geneticostsp;

import java.io.File;

public class AlgoritmoGeneticoComparacoes {
    private final File dirSaidaLogs;
    private final File dirSaidaRelatorios;

    public AlgoritmoGeneticoComparacoes(File dirSaidaLogs, File dirSaidaRelatorios) {
        this.dirSaidaLogs = dirSaidaLogs;
        this.dirSaidaRelatorios = dirSaidaRelatorios;
    }

    public void paralelismo1000Geracoes(MapaPontos mp, int[] valoresParalelismo,
            int comparacoes, String prefixoNomeArquivos, String textoDescricao) throws Exception {
        AlgoritmoGenetico[][] ags = new AlgoritmoGenetico[valoresParalelismo.length][];
        AlgoritmoGeneticoParametros p = new AlgoritmoGeneticoParametros();
        p.removerMaximoTempoMs();
        for (int i = 0; i < ags.length; i++) {
            p.setNumeroTarefasEmParalelo(valoresParalelismo[i]);
            ags[i] = AlgoritmoGenetico.multiplasExecucoes(mp, p, dirSaidaLogs, comparacoes);
        }
        AlgoritmoGeneticoRelatorios.relatoriosMultiplasComparacoes(dirSaidaRelatorios,
                prefixoNomeArquivos, textoDescricao + " - " + mp.nomeMapa, ags);
    }

    public void tamanhoPopulacao10minutos(MapaPontos mp, double[] valoresTamanhoPopulacao,
            int comparacoes, String prefixoNomeArquivos, String textoDescricao) throws Exception {
        AlgoritmoGenetico[][] ags = new AlgoritmoGenetico[valoresTamanhoPopulacao.length][];
        AlgoritmoGeneticoParametros p = new AlgoritmoGeneticoParametros();
        p.removerMaximoGeracoes();
        for (int i = 0; i < ags.length; i++) {
            p.setTamanhoPopulacaoRelativoAoTamanhoMapa(valoresTamanhoPopulacao[i]);
            ags[i] = AlgoritmoGenetico.multiplasExecucoes(mp, p, dirSaidaLogs, comparacoes);
        }
        AlgoritmoGeneticoRelatorios.relatoriosMultiplasComparacoes(dirSaidaRelatorios,
                prefixoNomeArquivos, textoDescricao + " - " + mp.nomeMapa, ags);
    }
    
    public void mutacao10minutos(MapaPontos mp, int[] valoresTipoMutacao,
            double[] valoresProbabilidadeMutacao, int comparacoes, String prefixoNomeArquivos,
            String textoDescricao) throws Exception {
        int n1 = valoresProbabilidadeMutacao.length;
        int n2 = valoresTipoMutacao.length;
        AlgoritmoGenetico[][] ags = new AlgoritmoGenetico[n1*n2][];
        AlgoritmoGenetico[][][] ags1 = new AlgoritmoGenetico[n1][n2][];
        AlgoritmoGenetico[][][] ags2 = new AlgoritmoGenetico[n2][n1][];
        
        AlgoritmoGeneticoParametros p = new AlgoritmoGeneticoParametros();
        p.removerMaximoGeracoes();
        for (int i = 0; i < n1; i++) {
            p.setProbMutacao(valoresProbabilidadeMutacao[i]);
            for (int j = 0; j < n2; j++) {
                p.setTipoMutacao(valoresTipoMutacao[j]);
                int z = i * n2 + j;
                ags[z] = AlgoritmoGenetico.multiplasExecucoes(mp, p, dirSaidaLogs, comparacoes);
                ags1[i][j] = ags[z];
                ags2[j][i] = ags[z];
           }
        }
        for (int i = 0; i < n1; i++) {
            double z = valoresProbabilidadeMutacao[i];
            AlgoritmoGeneticoRelatorios.relatoriosMultiplasComparacoes(dirSaidaRelatorios,
                    prefixoNomeArquivos + "-Pm" + z,
                    textoDescricao + " - " + mp.nomeMapa + " - pm = " + z, ags1[i]);
        }
        for (int j = 0; j < n2; j++) {
            int z = valoresTipoMutacao[j];
            AlgoritmoGeneticoRelatorios.relatoriosMultiplasComparacoes(dirSaidaRelatorios,
                    prefixoNomeArquivos + "-Mt" + z,
                    textoDescricao + " - " + mp.nomeMapa + " - mt #" + z, ags2[j]);
        }
        AlgoritmoGeneticoRelatorios.relatoriosMultiplasComparacoes(dirSaidaRelatorios,
                prefixoNomeArquivos, textoDescricao + " - " + mp.nomeMapa, ags);
        
        String[] titulosLinhas = new String[n1];
        for (int i = 0; i < n1; i++) {
            titulosLinhas[i] = "Probabilidade muta��o ";
            titulosLinhas[i] += valoresProbabilidadeMutacao[i];
        }
        String[] titulosColunas = new String[n2];
        for (int j = 0; j < n2; j++) {
            titulosColunas[j] = "Muta��o ";
            titulosColunas[j] += valoresTipoMutacao[j];
        }
        
        AlgoritmoGeneticoRelatorios.tabelaLatexDistanciasGeracoes(dirSaidaRelatorios,
                prefixoNomeArquivos, titulosColunas, titulosLinhas, ags);
    }

    public void tipoSNCrossover10minutos(MapaPontos mp, int[][] valoresTipoSNCrossover,
            int comparacoes, String prefixoNomeArquivos, String textoDescricao) throws Exception {
        AlgoritmoGenetico[][] ags = new AlgoritmoGenetico[valoresTipoSNCrossover.length][];
        AlgoritmoGeneticoParametros p = new AlgoritmoGeneticoParametros();
        p.removerMaximoGeracoes();
        for (int i = 0; i < ags.length; i++) {
            p.setTipoSelecaoNatural(valoresTipoSNCrossover[i][0]);
            p.setTipoCrossover(valoresTipoSNCrossover[i][1]);
            ags[i] = AlgoritmoGenetico.multiplasExecucoes(mp, p, dirSaidaLogs, comparacoes);
        }
        AlgoritmoGeneticoRelatorios.relatoriosMultiplasComparacoes(dirSaidaRelatorios,
                prefixoNomeArquivos, textoDescricao + " - " + mp.nomeMapa, ags);
    }

    public void mortalidadesSN10minutos(MapaPontos mp, double[][] valoresMortalidadesSN,
            int comparacoes, String prefixoNomeArquivos, String textoDescricao) throws Exception {
        AlgoritmoGenetico[][] ags = new AlgoritmoGenetico[valoresMortalidadesSN.length][];
        AlgoritmoGeneticoParametros p = new AlgoritmoGeneticoParametros();
        p.removerMaximoGeracoes();
        for (int i = 0; i < ags.length; i++) {
            p.setMortalidadeSNAntesReproducao(valoresMortalidadesSN[i][0]);
            p.setMortalidadeSNAposReproducao(valoresMortalidadesSN[i][1]);
            ags[i] = AlgoritmoGenetico.multiplasExecucoes(mp, p, dirSaidaLogs, comparacoes);
        }
        AlgoritmoGeneticoRelatorios.relatoriosMultiplasComparacoes(dirSaidaRelatorios,
                prefixoNomeArquivos, textoDescricao + " - " + mp.nomeMapa, ags);
    }

    public AlgoritmoGenetico aplicacao5000Geracoes(MapaPontos mp, String prefixoNomeArquivos,
            String textoDescricao) throws Exception {
        AlgoritmoGeneticoParametros p = new AlgoritmoGeneticoParametros();
        p.removerMaximoTempoMs();
        p.setMaximoGeracoes(5000);
        AlgoritmoGenetico ag = new AlgoritmoGenetico(mp, p, dirSaidaLogs);
        AlgoritmoGeneticoRelatorios.relatoriosComparacaoSimples(dirSaidaRelatorios,
                prefixoNomeArquivos, textoDescricao + " - " + mp.nomeMapa, ag);
        return ag;
    }
}
