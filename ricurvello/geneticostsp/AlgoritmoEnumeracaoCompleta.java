package ricurvello.geneticostsp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;

import ricurvello.geneticostsp.MapaPontos.Caminho;

public class AlgoritmoEnumeracaoCompleta {
    public final MapaPontos m;
    
    // Ordem dos pontos do mapa para o caminho que est� sendo constru�do e avaliado e indicador de
    // utiliza��o auxiliar:
    private final int[] ordemPontos;
    private final boolean[] indicadorUtilizacao;
    
    // Array auxiliar para a constru��o de caminhos:
    private double melhorDistanciaParcial = Double.MAX_VALUE;
    
    // Resultado da busca:
    private final int[] ordemPontosMelhorCaminho;
    public final Caminho caminhoOtimo;
    public final double distanciaCaminhoOtimo;
    public final String descricaoComResultado;
    
    // Acompanhamento do processamento:
    private long caminhosAvaliados;
    private long totalCaminhosAAvaliar;
    private PrintStream arquivoLog;
    public final Date inicio;
    public final Date fim;
    public final String tempoGasto;

    /**
     * Construtor que realiza a busca pela solu��o �tima por enumera��o completa dos caminhos
     * (forca-bruta)
     */
    public AlgoritmoEnumeracaoCompleta(MapaPontos m, File diretorioLog)
            throws FileNotFoundException {
        this.m = m;
        this.caminhosAvaliados = 0;
        if (this.m.numeroPontos < 20) {
            long fat = 1;
            for (int z = 2; z < this.m.numeroPontos; z++) {
                fat = fat * z;
            }
            this.totalCaminhosAAvaliar = fat;
        } else {
            this.totalCaminhosAAvaliar = -1; // valor n�o "cabe" em um long
        }
        this.ordemPontos = new int[this.m.numeroPontos];
        this.indicadorUtilizacao = new boolean[this.m.numeroPontos];
        this.ordemPontosMelhorCaminho = new int[this.m.numeroPontos];
        this.ordemPontos[0] = 0;
        this.indicadorUtilizacao[0] = true;
        for (int i = 1; i < this.m.numeroPontos; i++) {
            indicadorUtilizacao[i] = false;
        }
        
        // Informa��es de in�cio do processamento:
        this.inicio = new Date();
        String nomeArqLog = UtilFormatos.formatarDataSemBarras(inicio);
        nomeArqLog += "-" + m.nomeMapa + "-enumeracaocompleta-log.txt";
        this.arquivoLog = new PrintStream(new File(diretorioLog, nomeArqLog));
        String txt = UtilFormatos.formatarData(this.inicio);
        txt += ": iniciado algoritmo enumera��o completa no mapa \"" + this.m.nomeMapa + "\"";
        System.out.println(txt);
        this.arquivoLog.println(txt);
        
        // Buscar solu��o:
        this.buscarSolucaoRec(1);
        this.distanciaCaminhoOtimo = melhorDistanciaParcial;
        this.caminhoOtimo = m.gerarNovoCaminho(this.ordemPontosMelhorCaminho);
        this.fim = new Date();
        this.tempoGasto = UtilFormatos.formatarIntervalo(inicio, fim);
        this.descricaoComResultado = this.m.nomeMapa + ": dist�ncia " + this.distanciaCaminhoOtimo
                + " em " + this.tempoGasto + ". " + this.caminhoOtimo;
        
        // Informa��es de fim do processamento:
        txt = UtilFormatos.formatarData(this.fim) + " (" + this.tempoGasto + "): terminado ";
        txt += "algoritmo enumera��o completa no mapa \"" + this.m.nomeMapa + "\" - resultado: ";
        txt += this.caminhoOtimo + " com distancia = " + this.distanciaCaminhoOtimo;
        System.out.println(txt);
        this.arquivoLog.println(txt);
        this.arquivoLog.close();
    }

    /**
     * M�todo recursivo auxiliar para buscar o caminho minimo.
     */
    private void buscarSolucaoRec(int proximaPosicao) {
        if (proximaPosicao == this.m.numeroPontos) {
            // O caminho est� completo, avaliar a distancia:
            double distanciaTotal = this.m.getDistanciaAssociada(ordemPontos);
            if (distanciaTotal < this.melhorDistanciaParcial) {
                // Caminho atual deve ser copiado para o melhor caminho:
                for (int i = 0; i < this.m.numeroPontos; i++) {
                    this.ordemPontosMelhorCaminho[i] = this.ordemPontos[i];
                }
                this.melhorDistanciaParcial = distanciaTotal;
            }
            caminhosAvaliados++;
            // Informa��es durante o processamento:
            if ((caminhosAvaliados % 10000000) == 0) {
                Date agora = new Date();
                String txt = UtilFormatos.formatarData(agora) + " (";
                txt += UtilFormatos.formatarIntervalo(this.inicio, agora);
                txt += "): combina��o #" + caminhosAvaliados;
                if (this.totalCaminhosAAvaliar > 0) {
                    double porcentagem = 100.0 * caminhosAvaliados;
                    porcentagem = porcentagem / this.totalCaminhosAAvaliar;
                    txt += " de " + this.totalCaminhosAAvaliar + " (" + porcentagem + "%)";
                }
                txt += " do algoritmo enumera��o completa";
                System.out.println(txt);
                this.arquivoLog.println(txt);
            }
        } else {
            // O caminho ainda n�o est� completo:
            for (int i = 0; i < this.m.numeroPontos; i++) {
                if (this.indicadorUtilizacao[i] == false) {
                    this.indicadorUtilizacao[i] = true;
                    this.ordemPontos[proximaPosicao] = i;
                    this.buscarSolucaoRec(proximaPosicao + 1);
                    this.indicadorUtilizacao[i] = false;
                }
            }
        }
    }
    
    /**
     * M�todo que gera um relat�rio da execu��o em txt.
     */
    public void gerarRelatorio(File diretorio, String prefixoNomeArquivo)
            throws FileNotFoundException {
        String nomeArq = prefixoNomeArquivo + "-" + m.nomeMapa + ".txt";
        PrintStream ps = new PrintStream(new File(diretorio, nomeArq));
        String data = UtilFormatos.formatarData(this.fim);
        ps.println("Resultado gerado em " + data + ":");
        ps.println(this.descricaoComResultado);
        ps.close();
    }
    
    /**
     * M�todo que gera uma imagem do caminho �timo no mapa em png.
     */
    public void gerarImagemMelhorCaminhoNoMapa(File diretorio, String prefixoNomeArquivo)
            throws IOException {
        String nomeArq = prefixoNomeArquivo + "-" + m.nomeMapa + ".png";
        try {
            UtilGraficos.gravarPngCaminhoNoMapa(caminhoOtimo,
                    "caminho �timo da enumera��o completa", new File(diretorio, nomeArq));
        } catch (NoClassDefFoundError e) {
            System.err.println("Depend�ncias necess�rias n�o encontradas " + 
                    "(http://www.jfree.org/jfreechart/)");
            e.printStackTrace();
        }
    }
}
