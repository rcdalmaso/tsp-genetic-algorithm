package ricurvello.geneticostsp;

import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ThreadLocalRandom;

import ricurvello.geneticostsp.MapaPontos.Caminho;

public class AlgoritmoGenetico {
    public final MapaPontos m;
    public final AlgoritmoGeneticoParametros p;

    private Populacao populacao;
    private GerenciadorCrossoverEMutacao gerenciadorCrossoverEMutacao;

    private final SelecaoNatural implSelecaoNatural;
    private final Crossover implCrossover;
    private final Mutacao implMutacao;

    public final Caminho melhorCaminho;
    public final double melhorDistancia;
    public final long totalGeracoesProcessamento;
    public final String descricao;
    public final String descricaoComResultado;
    public final String descricaoCompacta;
    public final List<Double> distanciaOtimaPorGeracao;
    public final List<Long> tempoGastoPorGeracao;

    private long totalIndividuosGerados = 0;
    private long totalMutacoesRealizadas = 0;

    // Acompanhamento do processamento:
    private PrintStream arquivoLog;
    public final Date inicio;
    public final Date fim;
    private Date momentoMelhorCaminho;
    private long geracaoMelhorCaminho;
    public final String tempoAteEncontrarOtimo;
    public final long geracaoOtimoEncontrado;
    public final String tempoGasto;
    public final double tempoGastoSegundos;

    /**
     * Construtor do algoritmo gen�tico que j� realiza a busca pela solu��o.
     */
    public AlgoritmoGenetico(MapaPontos m, AlgoritmoGeneticoParametros p, File diretorioLog)
            throws Exception {
        this.m = m;
        this.p = (AlgoritmoGeneticoParametros) p.clone();
        double z = m.numeroPontos * this.p.getTamanhoPopulacaoRelativoAoTamanhoMapa();
        z = Math.ceil(z);
        if (z > Integer.MAX_VALUE) {
            z = Integer.MAX_VALUE;
        }
        final int N = (int) z;

        // Valida��o do crit�rio de parada:
        final long criterioParadaGeracoes = this.p.getMaximoGeracoes();
        final long criterioParadaTempo = this.p.getMaximoTempoMs();
        if (criterioParadaGeracoes == Long.MAX_VALUE && criterioParadaTempo == Long.MAX_VALUE) {
            throw new IllegalArgumentException("Crit�rio de parada inv�lido.");
        }

        // Informa��es de in�cio do processamento:
        this.inicio = new Date();
        String nomeArqLog = UtilFormatos.formatarDataSemBarras(inicio) + "-";
        nomeArqLog += m.nomeMapa + "-genetico-" + this.p.toStringCompacto();
        nomeArqLog += "-log.txt";
        this.arquivoLog = new PrintStream(new File(diretorioLog, nomeArqLog));
        String txt = UtilFormatos.formatarData(this.inicio);
        txt += ": iniciado algoritmo gen�tico no mapa \"" + this.m.nomeMapa + "\" com " + this.p;
        System.out.println(txt);
        this.arquivoLog.println(txt);

        // C�lculo do n�mero de indiv�duos a gerar por gera��o:
        double mortSN1 = this.p.getMortalidadeSNAntesReproducao();
        double mortSN2 = this.p.getMortalidadeSNAposReproducao();
        int numeroPais = (int) Math.round((1.0 - mortSN1) * N);
        int numeroSobreviventes = (int) Math.round((1.0 - mortSN2) * N);
        int numeroFilhos = N - numeroSobreviventes;

        // Construcao da implementa��o das etapas do algoritmo gen�tico:
        this.implSelecaoNatural = this.construirImplementacaoSelecaoNatural();
        this.implCrossover = this.construirImplementacaoCrossover();
        this.implMutacao = this.construirImplementacaoMutacao();

        // Processo iterativo do algoritmo gen�tico:

        // 1. Geracao da popula��o inicial com N individuos:
        this.populacao = new Populacao();
        for (int i = 0; i < N; i++) {
            populacao.adicionarIndividuo(m.gerarNovoCaminhoAleatorio());
            // Obs: no conjunto "populacao" sao adicionados N caminhos, mas os clones (caminhos
            // iguais) sao automaticamente ignorados.
        }
        this.gerenciadorCrossoverEMutacao = new GerenciadorCrossoverEMutacao();
        Caminho melhorCaminhoParcial = this.populacao.melhorCaminho;
        double melhorDistanciaParcial = this.populacao.melhorFitness;
        Date agora = new Date();

        // Informa�oes para log:
        txt = UtilFormatos.formatarData(agora) + " (";
        txt += UtilFormatos.formatarIntervalo(inicio, agora);
        txt += "): gera��o #0 do algoritmo gen�tico - melhor caminho achado na geracao inicial - ";
        txt += melhorCaminhoParcial + " - distancia = " + melhorDistanciaParcial;
        System.out.println(txt);
        this.arquivoLog.println(txt);
        this.momentoMelhorCaminho = agora;
        this.geracaoMelhorCaminho = 0;

        this.distanciaOtimaPorGeracao = new ArrayList<Double>();
        this.tempoGastoPorGeracao = new ArrayList<Long>();
        // Adiciona na posi��o 0 das duas listas:
        this.distanciaOtimaPorGeracao.add(melhorDistanciaParcial);
        this.tempoGastoPorGeracao.add(agora.getTime() - inicio.getTime());

        // 2. Processo iterativo para demais geracoes:
        int g = 1;
        while ((g <= criterioParadaGeracoes) &&
                ((agora.getTime() - inicio.getTime()) <= criterioParadaTempo)) {

            // 2.1. Sele��o Natural (sele��o do conjunto de individuos que se reproduzir�o e que
            // sobreviver�o):
            this.implSelecaoNatural.selecionarIndividuos(numeroPais, numeroSobreviventes);

            // 2.2. Reprodu��o (multiplica��o) com crossover e muta��o:
            this.gerenciadorCrossoverEMutacao.executarCrossoverEMutacao(g, numeroFilhos);

            // 2.3. Verificar se foi encontrado caminho melhor do que o �timo:
            Caminho melhorCaminhoDestaGeracao = this.populacao.melhorCaminho;
            double melhorDistanciaDestaGeracao = this.populacao.melhorFitness;
            agora = new Date();
            if (melhorDistanciaDestaGeracao < melhorDistanciaParcial) {
                melhorCaminhoParcial = melhorCaminhoDestaGeracao;
                melhorDistanciaParcial = melhorDistanciaDestaGeracao;

                // Informa�oes para log:
                txt = UtilFormatos.formatarData(agora) + " (";
                txt += UtilFormatos.formatarIntervalo(inicio, agora) + "): gera��o #" + g;
                if (criterioParadaGeracoes != Long.MAX_VALUE) {
                    txt += " de " + criterioParadaGeracoes + " (";
                    txt += ((100.0 * g) / (1.0 * criterioParadaGeracoes)) + "%)";
                }
                txt += " do algoritmo gen�tico - achado caminho melhor do que os encontrados ";
                txt += "anteriormente - " + melhorCaminhoParcial + " - distancia = ";
                txt += melhorDistanciaParcial;
                System.out.println(txt);
                this.arquivoLog.println(txt);
                this.momentoMelhorCaminho = agora;
                this.geracaoMelhorCaminho = g;
            } else {
                // Informa�oes para log:
                if ((g % 50) == 0) {
                    txt = UtilFormatos.formatarData(agora) + " (";
                    txt += UtilFormatos.formatarIntervalo(inicio, agora) + "): gera��o #" + g;
                    if (criterioParadaGeracoes != Long.MAX_VALUE) {
                        txt += " de " + criterioParadaGeracoes + " (";
                        txt += ((100.0 * g) / (1.0 * criterioParadaGeracoes)) + "%)";
                    }
                    txt += " do algoritmo gen�tico";
                    System.out.println(txt);
                    this.arquivoLog.println(txt);
                }
            }

            // 2.4. Registrar o melhor caminho at� esta gera��o na lista de dist�ncias por gera��o
            // e o tempo gasto:
            this.distanciaOtimaPorGeracao.add(melhorDistanciaParcial);
            this.tempoGastoPorGeracao.add(agora.getTime() - inicio.getTime());
            g++;
        }

        // 3. fim do processamento:
        this.melhorCaminho = melhorCaminhoParcial;
        this.melhorDistancia = melhorDistanciaParcial;
        this.fim = new Date();
        this.tempoGasto = UtilFormatos.formatarIntervalo(inicio, fim);
        txt = UtilFormatos.formatarData(this.fim) + " (" + this.tempoGasto + "): terminado ";
        txt += "algoritmo gen�tico no mapa \"" + this.m.nomeMapa + "\" com " + this.p;
        txt += " - resultado: " + this.melhorCaminho + " com distancia = " + this.melhorDistancia;
        txt += " - indiv�duos gerados = " + this.getTotalIndividuosGerados();
        txt += " - muta��es realizadas = " + this.getTotalMutacoesRealizadas();
        System.out.println(txt);
        this.arquivoLog.println(txt);
        this.arquivoLog.close();

        this.tempoAteEncontrarOtimo = UtilFormatos.formatarIntervalo(inicio, momentoMelhorCaminho);
        this.geracaoOtimoEncontrado = this.geracaoMelhorCaminho;

        this.totalGeracoesProcessamento = this.tempoGastoPorGeracao.size() - 1;
        this.tempoGastoSegundos = 0.001 * (fim.getTime() - inicio.getTime());
        this.descricao = m.nomeMapa + " com " + this.p;
        this.descricaoComResultado = m.nomeMapa + ": dist�ncia " + this.melhorDistancia
                + " na gera��o #" + this.geracaoOtimoEncontrado + " em "
                + this.tempoAteEncontrarOtimo + " (tempo total das "
                + this.totalGeracoesProcessamento + " gera��es = " + this.tempoGasto
                + "). Parametros: " + this.p + ". Indiv�duos gerados = "
                + this.getTotalIndividuosGerados() + ". Muta��es realizadas = "
                + this.getTotalMutacoesRealizadas() + ". Caminho = " + this.melhorCaminho;
        this.descricaoCompacta = m.nomeMapa + "-" + this.p.toStringCompacto();

        // Libera��o de recursos:
        this.populacao = null;
        this.gerenciadorCrossoverEMutacao.encerrar();
        this.gerenciadorCrossoverEMutacao = null;
    }

    /**
     * Interface interna que define o tipo de sele��o natural do algoritmo gen�tico.
     */
    private interface SelecaoNatural {
        /**
         * Seleciona indiv�duos que produzem descendentes e elimina os indiv�duos com pior fitness
         * (maior dist�ncia associada).
         */
        public void selecionarIndividuos(int numeroIndividuosQueGeramDescendentes,
                int numeroIndividuosQueSobrevivem);

        public Caminho[] sortearParPais();
    }

    /**
     * Interface interna que define o tipo de crossover do algoritmo gen�tico.
     */
    private interface Crossover {
        public Caminho crossover(Caminho[] parPais);
    }

    /**
     * Interface interna que define o tipo de muta��o do algoritmo gen�tico.
     */
    private interface Mutacao {
        public boolean mutacao(Caminho caminho, int numeroGeracao);
    }

    /**
     * Classe interna que implementa a sele��o natural "Elitista Uniforme":
     */
    private class SelecaoNaturalElitistaUniforme implements SelecaoNatural {
        List<Caminho> z;
        int n;

        public void selecionarIndividuos(int numPais, int numSobreviventes) {
            this.z = populacao.selecionarIndividuos(numPais, numSobreviventes);
            this.n = this.z.size();
        }

        public Caminho[] sortearParPais() {
            return new Caminho[] {
                    this.z.get(sortearInt(n)),
                    this.z.get(sortearInt(n)),
            };
        }
    }

    /**
     * Classe interna que implementa a sele��o natural "Elitista Triangular":
     */
    private class SelecaoNaturalElitistaTriangular implements SelecaoNatural {
        TreeMap<Long, Caminho> zProbAc;
        long n;

        public void selecionarIndividuos(int numPais, int numSobreviventes) {
            List<Caminho> z;
            z = populacao.selecionarIndividuos(numPais, numSobreviventes);
            long x = z.size();
            this.zProbAc = new TreeMap<Long, Caminho>();
            for (int i = 0; i < z.size(); i++) {
                this.zProbAc.put(x, z.get(i));
                x = x + (z.size() - i - 1);
            }
            n = x;
        }

        public Caminho[] sortearParPais() {
            return new Caminho[] {
                    this.zProbAc.get(this.zProbAc.higherKey(sortearLong(n))),
                    this.zProbAc.get(this.zProbAc.higherKey(sortearLong(n))),
            };
        }
    }

    /**
     * M�todo privado que define a implementa��o para a etapa de sele��o natural do algortimo
     * gen�tico.
     */
    private SelecaoNatural construirImplementacaoSelecaoNatural() {
        if (p.getTipoSelecaoNatural() == 1) { // "Elitista Uniforme":
            return new SelecaoNaturalElitistaUniforme();
        } else { // "Elitista Triangular":
            return new SelecaoNaturalElitistaTriangular();
        }
    }

    /**
     * M�todo privado que define a implementa��o para a etapa de crossover do algortimo gen�tico.
     */
    private Crossover construirImplementacaoCrossover() {
        if (p.getTipoCrossover() == 1) { // "Greedy Crossover":
            return new Crossover() {
                public Caminho crossover(Caminho[] parPais) {
                    return HeuristicasTSPGenericas.greedyCrossover(parPais);
                }
            };
        } else { // "Greedy Subtour Crossover (GSX)":
            return new Crossover() {
                public Caminho crossover(Caminho[] parPais) {
                    return HeuristicasTSPGenericas.greedySubtourCrossover(parPais);
                }
            };
        }
    }

    /**
     * M�todo privado que define a implementa��o para a etapa de muta��o do algortimo gen�tico.
     */
    private Mutacao construirImplementacaoMutacao() {
        if (p.getTipoMutacao() == 1) {
            // "Random Swap"
            return new Mutacao() {
                public boolean mutacao(Caminho caminho, int numeroGeracao) {
                    if (sortearDouble() < p.getProbMutacao()) {
                        return HeuristicasTSPGenericas.operacaoRandomSwap(caminho);
                    } else {
                        return false;
                    }
                }
            };
        } else if (p.getTipoMutacao() == 2) {
            // "Greedy Swap"
            return new Mutacao() {
                public boolean mutacao(Caminho caminho, int numeroGeracao) {
                    if (sortearDouble() < p.getProbMutacao()) {
                        return HeuristicasTSPGenericas.operacaoGreedySwap(caminho);
                    } else {
                        return false;
                    }
                }
            };
        } else if (p.getTipoMutacao() == 3) {
            // "2opt"
            return new Mutacao() {
                public boolean mutacao(Caminho caminho, int numeroGeracao) {
                    if (sortearDouble() < p.getProbMutacao()) {
                        return HeuristicasTSPGenericas.operacao2opt(caminho);
                    } else {
                        return false;
                    }
                }
            };
        } else if (p.getTipoMutacao() == 4) {
            // "Random Swap" + "2opt"
            return new Mutacao() {
                public boolean mutacao(Caminho caminho, int numeroGeracao) {
                    if (sortearDouble() < p.getProbMutacao()) {
                        boolean b1 = HeuristicasTSPGenericas.operacaoRandomSwap(caminho);
                        boolean b2 = HeuristicasTSPGenericas.operacao2opt(caminho);
                        return b1 || b2;
                    } else {
                        return false;
                    }
                }
            };
        } else {
            // "Random Swap" + "2opt" (20% gera��es) ou "2opt" (80% gera��es)
            return new Mutacao() {
                public boolean mutacao(Caminho caminho, int numeroGeracao) {
                    if (sortearDouble() < p.getProbMutacao()) {
                        boolean b1 = false;
                        if (numeroGeracao % 5 == 0) {
                            b1 = HeuristicasTSPGenericas.operacaoRandomSwap(caminho);
                        }
                        boolean b2 = HeuristicasTSPGenericas.operacao2opt(caminho);
                        return b1 || b2;
                    } else {
                        return false;
                    }
                }
            };
        }
    }

    /**
     * Classe interna que representa a popula��o ordenada por fitness.
     * Cada indiv�duo da popula��o � um caminho no mapa.
     */
    private class Populacao {
        private final TreeMap<Double, Set<Caminho>> p;
        private int n;
        private Caminho melhorCaminho;
        private double melhorFitness;

        private Populacao() {
            this.p = new TreeMap<Double, Set<Caminho>>();
            this.melhorCaminho = null;
            this.melhorFitness = Double.MAX_VALUE;
        }

        public synchronized void adicionarIndividuo(Caminho c) {
            double distancia = m.getDistanciaAssociada(c.ordem);
            Set<Caminho> z = p.get(distancia);
            if (z == null) {
                z = new HashSet<Caminho>();
                p.put(distancia, z);
            }
            if (z.add(c)) {
                this.n++;
                if (this.melhorCaminho == null || distancia < melhorFitness) {
                    this.melhorCaminho = c;
                    this.melhorFitness = distancia;
                }
            }
        }

        /**
         * M�todo que gera uma lista ordenada por fitness dos melhores indiv�duos da popula��o,
         * eliminando no processo os indiv�duos com pior fitness.
         * 
         * O par�metro tamanhoMaximoListaARetornar especifica o n�mero de indiv�duos a adicionar
         * na lista retornada.
         * 
         * O par�metro numeroIndividuosMaximoAManterNaPopulacao especifica o tamanho da popula��o
         * ap�s o processamento.
         */
        private synchronized List<Caminho> selecionarIndividuos(int tamanhoMaximoListaARetornar,
                int numeroIndividuosMaximoAManterNaPopulacao) {
            int numeroIndividuosRestantesAdicionarLista;
            if (tamanhoMaximoListaARetornar > this.n) {
                numeroIndividuosRestantesAdicionarLista = this.n;
            } else {
                numeroIndividuosRestantesAdicionarLista = tamanhoMaximoListaARetornar;
            }
            int numeroIndividuosRestanteManterNaPopulacao;
            if (numeroIndividuosMaximoAManterNaPopulacao > this.n) {
                numeroIndividuosRestanteManterNaPopulacao = this.n;
            } else {
                numeroIndividuosRestanteManterNaPopulacao =
                        numeroIndividuosMaximoAManterNaPopulacao;
            }
            if (numeroIndividuosRestanteManterNaPopulacao == 0) {
                this.melhorCaminho = null;
                this.melhorFitness = Double.MAX_VALUE;
            }
            List<Caminho> l = new ArrayList<Caminho>(numeroIndividuosRestantesAdicionarLista);
            Iterator<Double> itd = this.p.keySet().iterator();
            while (itd.hasNext()) {
                Double d = itd.next();
                if (numeroIndividuosRestantesAdicionarLista > 0
                        || numeroIndividuosRestanteManterNaPopulacao > 0) {
                    Set<Caminho> z = this.p.get(d);
                    Iterator<Caminho> itc = z.iterator();
                    while (itc.hasNext()) {
                        Caminho c = itc.next();
                        if (numeroIndividuosRestantesAdicionarLista > 0) {
                            l.add(c);
                            numeroIndividuosRestantesAdicionarLista--;
                        }
                        if (numeroIndividuosRestanteManterNaPopulacao > 0) {
                            numeroIndividuosRestanteManterNaPopulacao--;
                        } else {
                            itc.remove();
                            this.n--;
                        }
                    }
                } else {
                    itd.remove();
                }
            }
            return l;
        }
    }

    /**
     * Classe interna que gerencia as operacoes de crossover e muta��o com paralelismo.
     */
    private class GerenciadorCrossoverEMutacao {
        private final int paralelismo;
        private final ThreadExecucaoCrossoverMutacao[] threads;
        private int numeroGeracao;
        private int filhosEmGeracao;
        private int filhosAGerar;

        // Construtor:
        public GerenciadorCrossoverEMutacao() {
            this.paralelismo = p.getNumeroTarefasEmParalelo();
            if (paralelismo == 1) {
                this.threads = null;
            } else {
                this.threads = new ThreadExecucaoCrossoverMutacao[paralelismo];
                for (int i = 0; i < paralelismo; i++) {
                    this.threads[i] = new ThreadExecucaoCrossoverMutacao();
                }
            }
        }

        // M�todo que executa as opera��es:
        public void executarCrossoverEMutacao(int numeroGeracao, int numeroFilhosAGerar)
                throws InterruptedException {
            this.numeroGeracao = numeroGeracao;
            this.filhosAGerar = numeroFilhosAGerar;
            if (this.paralelismo == 1) {
                // Sem paralelismo:
                while (this.filhosAGerar > 0) {
                    populacao.adicionarIndividuo(gerarFilho());
                    this.filhosAGerar--;
                }
            } else {
                // Com paralelismo:
                this.filhosEmGeracao = 0;
                synchronized (this) {
                    this.notifyAll();
                    // Aguardar execucao das Threads abertas:
                    while ((filhosEmGeracao + filhosAGerar) > 0) {
                        this.wait();
                    }
                }
            }
        }

        // Metodo que encerra as Threads iniciadas no construtor:
        private void encerrar() {
            if (this.threads != null) {
                for (int i = 0; i < this.paralelismo; i++) {
                    this.threads[i].t.interrupt();
                }
            }
        }

        // Metodo centralizador para as operacoes de crossover e mutacao:
        private Caminho gerarFilho() {
            // 1. Sortear 2 pais:
            Caminho[] pais = implSelecaoNatural.sortearParPais();

            // 2. Realizar crossover:
            Caminho filho = implCrossover.crossover(pais);
            registrarIndividuoGerado();

            // 3. Realizar mutacao:
            boolean b = implMutacao.mutacao(filho, this.numeroGeracao);
            if (b) {
                registrarMutacaoRealizada();
            }

            // 4. Retornar o filho gerado:
            return filho;
        }

        // Threads de execucao do crossover e mutacao quando em paralelismo:
        private class ThreadExecucaoCrossoverMutacao implements Runnable {
            private final Thread t;

            public ThreadExecucaoCrossoverMutacao() {
                this.t = new Thread(this);
                this.t.start();
            }

            public void run() {
                while (true) {
                    // 1. Verificar a necessidade de geracao de novo filho:
                    boolean gerarNovoFilho;
                    synchronized (GerenciadorCrossoverEMutacao.this) {
                        if (filhosAGerar > 0) {
                            filhosAGerar--;
                            filhosEmGeracao++;
                            gerarNovoFilho = true;
                        } else {
                            gerarNovoFilho = false;
                        }
                    }
                    if (gerarNovoFilho) {
                        // 2. Gerar novo filho:
                        populacao.adicionarIndividuo(gerarFilho());
                        // 3. Registrar o fim do processamento:
                        synchronized (GerenciadorCrossoverEMutacao.this) {
                            filhosEmGeracao--;
                            if ((filhosEmGeracao + filhosAGerar) == 0) {
                                GerenciadorCrossoverEMutacao.this.notifyAll();
                            }
                        }
                    } else {
                        synchronized (GerenciadorCrossoverEMutacao.this) {
                            try {
                                GerenciadorCrossoverEMutacao.this.wait();
                            } catch (InterruptedException e) {
                                // Thread encerrada.
                                return;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * M�todo privado auxiliar para sortear um double entre 0 e 1.
     */
    private double sortearDouble() {
        return ThreadLocalRandom.current().nextDouble();
    }

    /**
     * M�todo privado auxiliar para sortear um int entre 0 e n - 1 dado n.
     */
    private int sortearInt(int n) {
        return ThreadLocalRandom.current().nextInt(n);
    }

    /**
     * M�todo privado auxiliar para sortear um long entre 0 e n - 1 dado n.
     */
    private long sortearLong(long n) {
        return ThreadLocalRandom.current().nextLong(n);
    }

    /**
     * M�todo para contabilizar o total de indiv�duos gerados.
     */
    private synchronized void registrarIndividuoGerado() {
        this.totalIndividuosGerados++;
    }

    /**
     * M�todo para contabilizar o total de muta��es realizadas.
     */
    private synchronized void registrarMutacaoRealizada() {
        this.totalMutacoesRealizadas++;
    }

    /**
     * M�todo para obter o total de indiv�duos gerados.
     */
    public synchronized long getTotalIndividuosGerados() {
        return totalIndividuosGerados;
    }

    /**
     * M�todo para obter o total de muta��es realizadas.
     */
    public synchronized long getTotalMutacoesRealizadas() {
        return totalMutacoesRealizadas;
    }
    
    /**
     * M�todo utilit�rio que realiza m�ltiplas execu��es do algoritmo gen�tico.
     */
    public static AlgoritmoGenetico[] multiplasExecucoes(MapaPontos m,
            AlgoritmoGeneticoParametros p, File diretorioLog, int numeroExecucoes)
            throws Exception {
        AlgoritmoGenetico[] ag = new AlgoritmoGenetico[numeroExecucoes];
        for (int i = 0; i < numeroExecucoes; i++) {
            ag[i] = new AlgoritmoGenetico(m, p, diretorioLog);
        }
        return ag;
    }
}
