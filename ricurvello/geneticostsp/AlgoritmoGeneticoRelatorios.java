package ricurvello.geneticostsp;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Date;
import java.util.List;

public class AlgoritmoGeneticoRelatorios {

    private static void relatoriosCvsTxt(File dir, String prefixoArquivos, AlgoritmoGenetico[] ag)
            throws IOException {
        Date agora = new Date();
        System.out.print("Compara��o de resultados gerados em ");
        System.out.println(UtilFormatos.formatarData(agora) + ":");
        System.out.print("Distribui��o da dist�ncia do melhor caminho por ");
        System.out.println("gera��o:");
        File arqCvs = new File(dir, nomeArquivo(prefixoArquivos, ".csv", ag));
        File arqTxt = new File(dir, nomeArquivo(prefixoArquivos, ".txt", ag));
        PrintStream psCsv = new PrintStream(arqCvs);
        PrintStream psTxt = new PrintStream(arqTxt);
        psCsv.println("sep=,");
        psCsv.print("# Geracao,");
        psTxt.print("Resumo de resultados gerado em ");
        psTxt.println(UtilFormatos.formatarData(agora) + ":");
        int maxN = 0;
        for (int i = 0; i < ag.length; i++) {
            int N = ag[i].distanciaOtimaPorGeracao.size();
            if (N > maxN) {
                maxN = N;
            }
            System.out.println("A" + i + " = " + ag[i].descricao);
            psCsv.print(ag[i].descricao);
            psCsv.print(",");
        }
        psCsv.println();
        System.out.print("Ger#");
        for (int i = 0; i < ag.length; i++) {
            System.out.print(" - A" + i);
        }
        System.out.println();
        for (int n = 0; n < maxN; n++) {
            System.out.print(n);
            psCsv.print(n);
            psCsv.print(",");
            for (int i = 0; i < ag.length; i++) {
                System.out.print(" - ");
                if (ag[i].distanciaOtimaPorGeracao.size() > n) {
                    System.out.print(ag[i].distanciaOtimaPorGeracao.get(n));
                    psCsv.print(ag[i].distanciaOtimaPorGeracao.get(n));
                    psCsv.print(",");
                } else {
                    System.out.print("x");
                    psCsv.print(",");
                }
            }
            System.out.println();
            psCsv.println();
        }
        System.out.println("Resumo:");
        for (AlgoritmoGenetico z : ag) {
            System.out.println(z.descricaoComResultado);
            psTxt.println(z.descricaoComResultado);
        }
        psCsv.close();
        psTxt.close();
    }

    public static void relatoriosComparacaoSimples(File dir, String prefixoArquivos,
            String textoDescricao, AlgoritmoGenetico... ag) throws IOException {

        //  Relat�rios cvs e txt:
        relatoriosCvsTxt(dir, prefixoArquivos, ag);
        
        // Imagens dos caminhos nos mapas:
        try {
            for (AlgoritmoGenetico a : ag) {
                String id = a.p.toStringCompacto();
                File f = new File(dir, nomeArquivo(prefixoArquivos, "-" + id + ".png", ag));
                UtilGraficos.gravarPngCaminhoNoMapa(a.melhorCaminho, a.descricaoCompacta, f);
            }
        } catch (NoClassDefFoundError e) {
            System.err.println("Depend�ncias necess�rias n�o encontradas "
                    + "(http://www.jfree.org/jfreechart/)");
            e.printStackTrace();
        }
        
        // Gr�ficos da evolu��o do algoritmo:
        try {
            File f = new File(dir, nomeArquivo(prefixoArquivos, "-geracoes.png", ag));
            UtilGraficos.gravarPngEvolucaoAlgoritmoGenetico(ag, textoDescricao, f, false);
            f = new File(dir, nomeArquivo(prefixoArquivos, "-tempos.png", ag));
            UtilGraficos.gravarPngEvolucaoAlgoritmoGenetico(ag, textoDescricao, f, true);
        } catch (NoClassDefFoundError e) {
            System.err.println("Depend�ncias necess�rias n�o encontradas "
                    + "(http://www.jfree.org/jfreechart/)");
            e.printStackTrace();
        }
    }

    public static void relatoriosMultiplasComparacoes(File dir, String prefixoArquivos,
            String textoDescricao, AlgoritmoGenetico[][] ags) throws IOException {
        // Criar um array duplo com os indices invertidos:
        int n1 = ags.length;
        int n2 = ags[0].length;
        
        AlgoritmoGenetico[][] ags2 = new AlgoritmoGenetico[n2][n1];
        for (int i = 0; i < n1; i++) {
            for (int j = 0; j < n2; j++) {
                ags2[j][i] = ags[i][j];
            }
        }
        if(n2 > 1){
            for (int j = 0; j < n2; j++) {
                relatoriosComparacaoSimples(dir, prefixoArquivos + "-" + (j + 1),
                        textoDescricao + " (compara��o #" + (j + 1) + ")", ags2[j]);
            }
            
            Date agora = new Date();
            String sufixo = "-" + UtilFormatos.formatarDataSemBarras(agora);
            sufixo += "-multiplascomparacoes.txt";
            File arqTxt = new File(dir, nomeArquivo(prefixoArquivos, sufixo, ags));
            PrintStream psTxt = new PrintStream(arqTxt);
            psTxt.print("Multiplas comparacoes realizadas em ");
            psTxt.println(UtilFormatos.formatarData(agora) + ":");
            double[][] mediasEDesviosPadrao = calcularMediasEDesviosPadrao(ags);
            for (int i = 0; i < n1; i++) {
                psTxt.print(ags[i][0].descricao);
                psTxt.print(" - n = " + mediasEDesviosPadrao[i][0]);
                psTxt.print(" - distancia: media = " + mediasEDesviosPadrao[i][1]);
                psTxt.print(" desv.padrao = " + mediasEDesviosPadrao[i][2]);
                psTxt.print(" - total geracoes: media = " + mediasEDesviosPadrao[i][3]);
                psTxt.print(" desv.padrao = " + mediasEDesviosPadrao[i][4]);
                psTxt.print(" - total tempo segundos: media = " + mediasEDesviosPadrao[i][5]);
                psTxt.println(" desv.padrao = " + mediasEDesviosPadrao[i][6]);
            }
            psTxt.close();
        } else {
            relatoriosComparacaoSimples(dir, prefixoArquivos, textoDescricao, ags2[0]);
        }
    }

    public static void tabelaLatexDistanciasGeracoes(File dir, String prefixoArquivo,
            String[] titulosColunas, String[] titulosLinhas, AlgoritmoGenetico[][] ags)
            throws IOException {
        
        double[][] mediasEDesviosPadrao = calcularMediasEDesviosPadrao(ags);

        String primeiraCelula = nomeArquivo(prefixoArquivo, "", ags);
        if (titulosColunas.length * titulosLinhas.length != ags.length) {
            throw new IllegalArgumentException();
        }

        File arqTex = new File(dir, nomeArquivo(prefixoArquivo, ".tex", ags));
        PrintStream psTex = new PrintStream(arqTex);

        psTex.print("\\begin{tabular}{c");
        for (int i = 0; i < titulosColunas.length; i++) {
            psTex.print("c");
        }
        psTex.println("}");
        psTex.println("\\toprule");
        psTex.print(primeiraCelula);
        for (int i = 0; i < titulosColunas.length; i++) {
            psTex.print(" & ");
            psTex.print(titulosColunas[i]);
        }
        psTex.println("\\\\");
        psTex.print("\\midrule ");
        for (int j = 0; j < titulosLinhas.length; j++) {
            psTex.println("\\midrule");
            psTex.print(titulosLinhas[j]);
            for (int i = 0; i < titulosColunas.length; i++) {
                psTex.print(" & ");
                int z = j * titulosColunas.length + i;
                psTex.print(mediasEDesviosPadrao[z][1]);
                psTex.print("(\\sigma = ");
                psTex.print(mediasEDesviosPadrao[z][2]);
                psTex.print(") em");
            }
            psTex.println(" \\\\");
            psTex.print("     ");
            for (int i = 0; i < titulosColunas.length; i++) {
                psTex.print(" & ");
                int z = j * titulosColunas.length + i;
                psTex.print(mediasEDesviosPadrao[z][3]);
                psTex.print(" gera��es (\\sigma = ");
                psTex.print(mediasEDesviosPadrao[z][4]);
                psTex.print(")");
            }
            psTex.println(" \\\\");
            psTex.print("     ");
            for (int i = 0; i < titulosColunas.length; i++) {
                psTex.print(" & ");
                int z = j * titulosColunas.length + i;
                psTex.print("(n = ");
                psTex.print(mediasEDesviosPadrao[z][0]);
                psTex.print(")");
            }
            psTex.println(" \\\\");
        }
        psTex.println("\\bottomrule");
        psTex.println("\\end{tabular}");
        psTex.close();
    }

    public static void graficoTempoExecucaoTamanhoMapa(File dir, String prefixoArquivo,
            List<AlgoritmoGenetico> lag) throws IOException {
        AlgoritmoGenetico[] ag = lag.toArray(new AlgoritmoGenetico[0]);
        try {
            File f = new File(dir, nomeArquivo(prefixoArquivo, "-tempotamanho.png", ag));
            UtilGraficos.gravarPngRelacaoTempoTamanho(ag, f);
        } catch (NoClassDefFoundError e) {
            System.err.println("Depend�ncias necess�rias n�o encontradas "
                    + "(http://www.jfree.org/jfreechart/)");
            e.printStackTrace();
        }
    }
    
    private static String nomeArquivo(String prefixo, String sufixo, AlgoritmoGenetico[] ag) {
        boolean mesmoMapa = true;
        MapaPontos m = ag[0].m;
        for (int i = 1; i < ag.length; i++) {
            if (ag[i].m != m) {
                mesmoMapa = false;
                break;
            }
        }
        if (mesmoMapa) {
            return prefixo + "-" + m.nomeMapa + sufixo;
        } else {
            return prefixo + sufixo;
        }
    }

    private static String nomeArquivo(String prefixo, String sufixo, AlgoritmoGenetico[][] ags) {
        boolean mesmoMapa = true;
        MapaPontos m = ags[0][0].m;
        loop:
        for (int i = 0; i < ags.length; i++) {
            for (int j = 0; j < ags[i].length; j++) {
                if (ags[i][j].m != m) {
                    mesmoMapa = false;
                    break loop;
                }
            }
        }
        if (mesmoMapa) {
            return prefixo + "-" + m.nomeMapa + sufixo;
        } else {
            return prefixo + sufixo;
        }
    }

    /**
     * Retorna um array duplo de doubles com primeira dimensao igual � primeira dimens�o do array
     * de "AlgoritmoGenetico" fornecido e a segunda dimens�o igual a 7 elementos, representando:
     * 
     * d[k][0] = n�mero de compara��es realizadas para o conjunto de par�metros "k".
     * d[k][1] = m�dia das melhores dist�ncias para o conjunto de par�metros "k".
     * d[k][2] = desvio padr�o das melhores dist�ncias para o conjunto de par�metros "k".
     * d[k][3] = m�dia do n�mero de gera��es para o conjunto de par�metros "k".
     * d[k][4] = desvio padr�o do n�mero de gera��es para o conjunto de par�metros "k".
     * d[k][5] = m�dia do tempo gasto em segundos para o conjunto de par�metros "k".
     * d[k][6] = desvio padr�o do tempo gasto em segundos para o conjunto de par�metros "k".
     * 
     */
    private static double[][] calcularMediasEDesviosPadrao(AlgoritmoGenetico[][] ags) {
        double[][] d = new double[ags.length][7];
        int n;
        double[] aux1, aux2, aux3, aux4;
        for (int i = 0; i < d.length; i++) {
            n = ags[i].length;
            d[i][0] = n;
            aux1 = new double[n]; // melhores distancias
            aux2 = new double[n]; // total geracoes
            aux3 = new double[n]; // total tempo segundos
            for (int j = 0; j < n; j++) {
                aux1[j] = ags[i][j].melhorDistancia;
                aux2[j] = ags[i][j].totalGeracoesProcessamento;
                aux3[j] = ags[i][j].tempoGastoSegundos;
            }
            aux4 = calcularMediaDesvioPadrao(aux1);
            d[i][1] = aux4[0];
            d[i][2] = aux4[1];
            aux4 = calcularMediaDesvioPadrao(aux2);
            d[i][3] = aux4[0];
            d[i][4] = aux4[1];
            aux4 = calcularMediaDesvioPadrao(aux3);
            d[i][5] = aux4[0];
            d[i][6] = aux4[1];
        }
        return d;
    }

    private static double[] calcularMediaDesvioPadrao(double... d) {
        double n = d.length;
        if (n == 0.0) {
            return null;
        }
        double soma = 0.0;
        double somaQuadrados = 0.0;
        for (double x : d) {
            soma += x;
            somaQuadrados += (x * x);
        }
        double media = soma / n;
        double mediaQuadrados = somaQuadrados / n;
        double desvioPadrao = mediaQuadrados - (media * media);
        return new double[] { media, desvioPadrao };
    }
}
