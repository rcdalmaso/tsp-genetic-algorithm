package ricurvello.geneticostsp;

import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilFormatos {
    private static final SimpleDateFormat formatador1;
    private static final SimpleDateFormat formatador2;

    static {
        formatador1 = new SimpleDateFormat("dd/MM/yyyy-HH:mm:ss.SSS");
        formatador2 = new SimpleDateFormat("yyyyMMdd-HHmmss.SSS");
    }

    public static String formatarData(Date d) {
        return formatador1.format(d);
    }

    public static String formatarDataSemBarras(Date d) {
        return formatador2.format(d);
    }

    public static String formatarIntervalo(Date inicio, Date fim) {
        return formatarIntervalo(fim.getTime() - inicio.getTime());
    }

    public static String formatarIntervalo(long milisegundos) {
        int ms = (int) milisegundos % 1000;
        long segundos = milisegundos / 1000;
        int s = (int) segundos % 60;
        long minutos = segundos / 60;
        int m = (int) minutos % 60;
        long horas = minutos / 60;
        String str = ms + "ms";
        if (segundos > 0) {
            if (ms < 10) {
                str = "0" + str;
            }
            if (ms < 100) {
                str = "0" + str;
            }
            str = s + "s" + str;
            if (minutos > 0) {
                if (s < 10) {
                    str = "0" + str;
                }
                str = m + "min" + str;
                if (horas > 0) {
                    if (m < 10) {
                        str = "0" + str;
                    }
                    str = horas + "h" + str;
                }
            }
        }
        return str;
    }
}
