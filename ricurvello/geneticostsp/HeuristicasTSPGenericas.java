package ricurvello.geneticostsp;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.concurrent.ThreadLocalRandom;

import ricurvello.geneticostsp.MapaPontos.Caminho;

/**
 * Classe que define heur�sticas usadas em algoritmos de otimiza��o para o TSP, incluindo opera��es
 * a serem usadas no algoritmo gen�tico.
 */
public class HeuristicasTSPGenericas {

    /**
     * Realiza uma opera��o de crossover (gera um novo caminho a partir de 2 caminhos no mapa) para
     * o algoritmo gen�tico do tipo "Greedy Crossover".
     * 
     * "Greedy crossover selects the first city of one parent, compares the cities leaving that
     * city in both parents, and chooses the closer one to extend the tour. If one city has already
     * appeared in the tour, we choose the other city. If both cities have already appeared, we
     * randomly select a non-selected city."
     * 
     * O array fornecido como par�metro deve ter comprimento 2.
     */
    public static Caminho greedyCrossover(Caminho[] pais) {
        final MapaPontos mp = pais[0].getMapaPontosAssociado();
        final int N = mp.numeroPontos;
        final Caminho c = mp.gerarNovoCaminhoVazio();

        // Rela��o de posi��es ainda n�o usadas no caminho sendo constru�do:
        TreeSet<Integer> pontosAindaNaoUtilizados = new TreeSet<Integer>();
        for (int i = 1; i < N; i++) {
            pontosAindaNaoUtilizados.add(i);
        }
        c.ordem[0] = 0;

        // Rela��es de posi��es seguintes para cada posi��o dos pais:
        int[][] pontosSeguintesPais = new int[2][N];
        for (int i = 0; i < (N - 1); i++) {
            pontosSeguintesPais[0][pais[0].ordem[i]] = pais[0].ordem[i + 1];
            pontosSeguintesPais[1][pais[1].ordem[i]] = pais[1].ordem[i + 1];
        }
        pontosSeguintesPais[0][pais[0].ordem[N - 1]] = pais[0].ordem[0];
        pontosSeguintesPais[1][pais[1].ordem[N - 1]] = pais[1].ordem[0];

        // Constru��o da sequencia:
        int pontoAnterior = 0;
        for (int i = 1; i < N; i++) {
            boolean proximaPosicaoPai1Disponivel =
                    pontosAindaNaoUtilizados.contains(pontosSeguintesPais[0][pontoAnterior]);
            boolean proximaPosicaoPai2Disponivel = 
                    pontosAindaNaoUtilizados.contains(pontosSeguintesPais[1][pontoAnterior]);
            if (proximaPosicaoPai1Disponivel) {
                if (proximaPosicaoPai2Disponivel) {

                    // As pr�ximas posi��es dos 2 pais est�o disponiveis. Verificar qual � a menor:
                    double distanciaPai1 = mp.getDistancia(pontoAnterior,
                            pontosSeguintesPais[0][pontoAnterior]);
                    double distanciaPai2 = mp.getDistancia(pontoAnterior,
                            pontosSeguintesPais[1][pontoAnterior]);

                    if (distanciaPai1 <= distanciaPai2) {
                        // Escolhida pr�xima posi��o do pai 1:
                        c.ordem[i] = pontosSeguintesPais[0][pontoAnterior];
                    } else {
                        // Escolhida pr�xima posi��o do pai 2:
                        c.ordem[i] = pontosSeguintesPais[1][pontoAnterior];
                    }
                } else {

                    // Apenas pr�xima posi��o do pai 1 dispon�vel:
                    c.ordem[i] = pontosSeguintesPais[0][pontoAnterior];
                }
            } else {
                if (proximaPosicaoPai2Disponivel) {

                    // Apenas pr�xima posi��o do pai 2 dispon�vel:
                    c.ordem[i] = pontosSeguintesPais[1][pontoAnterior];
                } else {

                    // Nenhuma pr�xima posi��o dos pais est� dispon�vel.
                    // Sorteando uma ainda n�o usada:
                    int posicaoSorteada = ThreadLocalRandom.current().nextInt(
                            pontosAindaNaoUtilizados.size());
                    Iterator<Integer> it = pontosAindaNaoUtilizados.iterator();
                    int j = 0;
                    while (j < posicaoSorteada) {
                        it.next();
                        j++;
                    }
                    int valorSorteado = it.next();
                    c.ordem[i] = valorSorteado;
                }
            }
            pontoAnterior = c.ordem[i];
            pontosAindaNaoUtilizados.remove(c.ordem[i]);
        }
        return c;
    }

    /**
     * Realiza uma opera��o de crossover (gera um novo caminho a partir de 2 caminhos no mapa) para
     * o algoritmo gen�tico do tipo "Greedy Subtour Crossover (GSX)" (vers�o gen�tica mais pura,
     * que n�o usa informa��es de distancias do problema).
     * 
     * "We propose a new crossover operator that acquires the longest possible sequence of parents'
     * subtours. We named it 'Greedy Subtour Crossover (GSX)."
     * 
     * O array fornecido como par�metro deve ter comprimento 2.
     */
    public static Caminho greedySubtourCrossover(Caminho[] pais) {
        final MapaPontos mp = pais[0].getMapaPontosAssociado();
        final int N = mp.numeroPontos;
        final Caminho c = mp.gerarNovoCaminhoVazio();

        // Rela��o de posi��es ainda n�o usadas no caminho sendo constru�do:
        TreeSet<Integer> pontosAindaNaoUtilizados = new TreeSet<Integer>();
        for (int i = 1; i < N; i++) {
            pontosAindaNaoUtilizados.add(i);
        }
        int paiTrechoAntesCrossover = ThreadLocalRandom.current().nextInt(2);
        int paiTrechoDepoisCrossover = 1 - paiTrechoAntesCrossover;
        int pontoDeCrossover = 1 + ThreadLocalRandom.current().nextInt(N - 1);
        int posicaoCrossoverPaiAntes = 0;
        int posicaoCrossoverPaiDepois = 0;
        for (int i = 1; i < N; i++) {
            if (pais[paiTrechoAntesCrossover].ordem[i] == pontoDeCrossover) {
                posicaoCrossoverPaiAntes = i;
            }
            if (pais[paiTrechoDepoisCrossover].ordem[i] == pontoDeCrossover) {
                posicaoCrossoverPaiDepois = i;
            }
            if (posicaoCrossoverPaiAntes != 0 && posicaoCrossoverPaiDepois != 0) {
                break;
            }
        }
        pontosAindaNaoUtilizados.remove(pontoDeCrossover);
        LinkedList<Integer> sequenciaFilho = new LinkedList<Integer>();
        sequenciaFilho.addFirst(pontoDeCrossover);
        boolean expansaoParaTrasPossivel = true;
        boolean expansaoParaFrentePossivel = true;
        int posAntesCrossoverPreenchidaPaiAntes = posicaoCrossoverPaiAntes;
        int posAposCrossoverPreenchidaPaiDepois = posicaoCrossoverPaiDepois;
        while (expansaoParaTrasPossivel || expansaoParaFrentePossivel) {
            int aux_posicao;
            int aux_valor;

            // Tenta expandir para tr�s:
            if (expansaoParaTrasPossivel && posAntesCrossoverPreenchidaPaiAntes > 0) {
                aux_posicao = posAntesCrossoverPreenchidaPaiAntes - 1;
                aux_valor = pais[paiTrechoAntesCrossover].ordem[aux_posicao];
                if (pontosAindaNaoUtilizados.contains(aux_valor)) {
                    // Expande para tr�s:
                    posAntesCrossoverPreenchidaPaiAntes = aux_posicao;
                    pontosAindaNaoUtilizados.remove(aux_valor);
                    sequenciaFilho.addFirst(aux_valor);
                } else {
                    expansaoParaTrasPossivel = false;
                }
            }

            // Tenta expandir para frente:
            if (expansaoParaFrentePossivel && posAposCrossoverPreenchidaPaiDepois < N - 1) {
                aux_posicao = posAposCrossoverPreenchidaPaiDepois + 1;
                aux_valor = pais[paiTrechoDepoisCrossover].ordem[aux_posicao];
                if (pontosAindaNaoUtilizados.contains(aux_valor)) {
                    // Expande para frente:
                    posAposCrossoverPreenchidaPaiDepois = aux_posicao;
                    pontosAindaNaoUtilizados.remove(aux_valor);
                    sequenciaFilho.addLast(aux_valor);
                } else {
                    expansaoParaFrentePossivel = false;
                }
            }
            if (posAposCrossoverPreenchidaPaiDepois == N - 1) {
                expansaoParaFrentePossivel = false;
            }
        }

        // Completa os pontos que faltam no filho atrav�s de sorteio:
        int numeroPontosASortear = N - 1 - sequenciaFilho.size();
        int numeroPontosASortearAntes;
        int numeroPontosASortearDepois;
        if (posAntesCrossoverPreenchidaPaiAntes == 1) {
            // Sequ�ncia filha atingiu o ponto 0 pelo pai antes do crossover:
            numeroPontosASortearAntes = 0;
            numeroPontosASortearDepois = numeroPontosASortear;
        } else if (posAposCrossoverPreenchidaPaiDepois == N - 1) {
            // Sequencia filha atingiu o ponto 0 pelo pai depois do crossover:
            numeroPontosASortearAntes = numeroPontosASortear;
            numeroPontosASortearDepois = 0;
        } else {
            // Sequencia filha n�o atingiu o zero por nenhum dos pais: escolhe aleatoriamente a
            // quantidade de pontos a sortear antes e depois:
            numeroPontosASortearAntes =
                    ThreadLocalRandom.current().nextInt(numeroPontosASortear + 1);
            numeroPontosASortearDepois = numeroPontosASortear - numeroPontosASortearAntes;
        }
        ArrayList<Integer> pontosASortear = new ArrayList<Integer>();
        for (Integer i : pontosAindaNaoUtilizados) {
            pontosASortear.add(i);
        }
        int posicaoOrdem = 0;
        c.ordem[posicaoOrdem++] = 0;
        for (int i = 0; i < numeroPontosASortearAntes; i++) {
            int posicaoSorteada = ThreadLocalRandom.current().nextInt(pontosASortear.size());
            int valorSorteado = pontosASortear.remove(posicaoSorteada);
            c.ordem[posicaoOrdem++] = valorSorteado;
        }
        for (Integer i : sequenciaFilho) {
            c.ordem[posicaoOrdem++] = i;
        }
        for (int i = 0; i < numeroPontosASortearDepois; i++) {
            int posicaoSorteada = ThreadLocalRandom.current().nextInt(pontosASortear.size());
            int valorSorteado = pontosASortear.remove(posicaoSorteada);
            c.ordem[posicaoOrdem++] = valorSorteado;
        }
        return c;
    }

    /**
     * Realiza uma opera��o de "Random-Swap"
     * 
     * Random Swap - "swap the order of cities in a path"
     * 
     * Retorna true se o caminho foi modificado ou false caso contr�rio.
     */
    public static boolean operacaoRandomSwap(Caminho caminho) {
        if (caminho.ordem.length < 2) {
            return false;
        }
        // 1. Sorteio das posi��es:
        int i = 1 + ThreadLocalRandom.current().nextInt(caminho.ordem.length - 1);
        int j = i;
        while (j == i) {
            j = 1 + ThreadLocalRandom.current().nextInt(caminho.ordem.length - 1);
        }
        // 2. Troca das posi��es:
        int aux = caminho.ordem[i];
        caminho.ordem[i] = caminho.ordem[j];
        caminho.ordem[j] = aux;
        return true;
    }

    /**
     * Realiza uma opera��o de "Greedy-Swap"
     * 
     * Greedy Swap - "The basic idea of greedy-swap is to randomly select two cities from one
     * chromosome and swap them if the new (swapped) tour length is shorter than the old one."
     * 
     * Retorna true se o caminho foi modificado ou false caso contr�rio.
     */
    public static boolean operacaoGreedySwap(Caminho caminho) {
        MapaPontos mapa = caminho.getMapaPontosAssociado();
        if (caminho.ordem.length < 4) {
            return false;
        }
        // 1. Sorteio das posi��es (i e j armazenam as posi��es sorteadas):
        int i = 1 + ThreadLocalRandom.current().nextInt(caminho.ordem.length - 1);
        int j = i;
        while (j == i) {
            j = 1 + ThreadLocalRandom.current().nextInt(caminho.ordem.length - 1);
        }
        // 2. Calculo das novas distancias:
        double distanciaASubtrair = 0;
        double distanciaASomar = 0;
        // 2.1. N�o considera este bloco se as posi��es s�o vizinhas com i menor:
        if ((i + 1) != j) {
            // Arestas anteriores:
            distanciaASubtrair += mapa.getDistancia(caminho.ordem[j - 1], caminho.ordem[j]);
            distanciaASomar += mapa.getDistancia(caminho.ordem[j - 1], caminho.ordem[i]);
            // Arestas posteriores:
            if (i < (caminho.ordem.length - 1)) {
                distanciaASubtrair += mapa.getDistancia(caminho.ordem[i], caminho.ordem[i + 1]);
                distanciaASomar += mapa.getDistancia(caminho.ordem[j], caminho.ordem[i + 1]);
            } else {
                distanciaASubtrair += mapa.getDistancia(caminho.ordem[i], 0);
                distanciaASomar += mapa.getDistancia(caminho.ordem[j], 0);
            }
        }
        // 2.2. N�o considera este bloco se as posi��es s�o vizinhas com j menor:
        if ((j + 1) != i) {
            // Arestas anteriores:
            distanciaASubtrair += mapa.getDistancia(caminho.ordem[i - 1], caminho.ordem[i]);
            distanciaASomar += mapa.getDistancia(caminho.ordem[i - 1], caminho.ordem[j]);
            // Arestas posteriores:
            if (j < (caminho.ordem.length - 1)) {
                distanciaASubtrair += mapa.getDistancia(caminho.ordem[j], caminho.ordem[j + 1]);
                distanciaASomar += mapa.getDistancia(caminho.ordem[i], caminho.ordem[j + 1]);
            } else {
                distanciaASubtrair += mapa.getDistancia(caminho.ordem[j], 0);
                distanciaASomar += mapa.getDistancia(caminho.ordem[i], 0);
            }
        }
        // 3. Troca das posicoes se for o caso:
        if (distanciaASubtrair > distanciaASomar) {
            int aux = caminho.ordem[i];
            caminho.ordem[i] = caminho.ordem[j];
            caminho.ordem[j] = aux;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Realiza uma operacao 2opt.
     * 
     * Retorna true se o caminho foi modificado ou false caso contr�rio.
     */
    public static boolean operacao2opt(Caminho caminho) {
        MapaPontos mapa = caminho.getMapaPontosAssociado();
        if (caminho.ordem.length < 4) {
            return false;
        }
        boolean operacaoRealizada = false;
        for (int a = 0; a < (caminho.ordem.length - 2); a++) {
            int b = a + 1;
            int c = b + 1;
            while (c < caminho.ordem.length) {
                int d = c + 1;
                if (d == caminho.ordem.length) {
                    d = 0;
                }
                // 1. Verifica se a troca das arestas "ab" e "cd" pelas arestas "ac" e "bd" trar�
                // ganho:
                double dist_ab_cd = mapa.getDistancia(caminho.ordem[a], caminho.ordem[b])
                        + mapa.getDistancia(caminho.ordem[c], caminho.ordem[d]);
                double dist_ac_bd = mapa.getDistancia(caminho.ordem[a], caminho.ordem[c])
                        + mapa.getDistancia(caminho.ordem[b], caminho.ordem[d]);
                if (dist_ab_cd > dist_ac_bd) {
                    // Realiza a troca com invers�o da ordem entre as posicoes
                    // b e c:
                    LinkedList<Integer> subcaminhoInvertido = new LinkedList<Integer>();
                    for (int k = c; k >= b; k--) {
                        subcaminhoInvertido.addLast(caminho.ordem[k]);
                    }
                    for (int k = b; k <= c; k++) {
                        caminho.ordem[k] = subcaminhoInvertido.removeFirst();
                    }
                    operacaoRealizada = true;
                    // Refaz a busca a partir na posicao b+1:
                    c = b + 1;
                } else {
                    // Apenas continua a busca:
                    c++;
                }
            }
        }
        return operacaoRealizada;
    }

    /**
     * Heuristica que cria sucessivamente caminhos aleat�rios e compara com o melhor encontrado at�
     * ent�o.
     * 
     * Executa durante o tempo especificado, e retorna o melhor caminho achado.
     */
    public static Caminho buscarCaminhoAleatoriamente(MapaPontos mp, long tempoExecucaoMs) {
        Date inicio = new Date();
        Caminho melhorC = mp.gerarNovoCaminhoAleatorio();
        double melhorD = mp.getDistanciaAssociada(melhorC.ordem);
        Date agora = new Date();
        Caminho c;
        double d;
        while ((agora.getTime() - inicio.getTime()) < tempoExecucaoMs) {
            c = mp.gerarNovoCaminhoAleatorio();
            d = mp.getDistanciaAssociada(c.ordem);
            if (d < melhorD) {
                melhorC = c;
            }
            agora = new Date();
        }
        return melhorC;
    }

    /**
     * Heuristica que cria sucessivamente caminhos aleat�rios, executa o 2opt no caminho gerado e
     * compara com o melhor encontrado at� ent�o.
     * 
     * Executa durante o tempo especificado, e retorna o melhor caminho achado.
     */
    public static Caminho buscarCaminho2opt(MapaPontos mp, long tempoExecucaoMs) {
        Date inicio = new Date();
        Caminho melhorC = mp.gerarNovoCaminhoAleatorio();
        operacao2opt(melhorC);
        double melhorD = mp.getDistanciaAssociada(melhorC.ordem);
        Date agora = new Date();
        Caminho c;
        double d;
        while ((agora.getTime() - inicio.getTime()) < tempoExecucaoMs) {
            c = mp.gerarNovoCaminhoAleatorio();
            operacao2opt(c);
            d = mp.getDistanciaAssociada(c.ordem);
            if (d < melhorD) {
                melhorC = c;
            }
            agora = new Date();
        }
        return melhorC;
    }

    /**
     * Realiza busca aleat�ria e busca com 2opt em um mapa durante 1s e 10min.
     * 
     * Retorna um relatorio em texto da busca realizada e os melhores caminhos encontrados.
     */
    public static String gerarRelatorioComparativoBuscas(MapaPontos mp) {
        Caminho c;
        StringBuffer sb = new StringBuffer("Mapa ");
        sb.append(mp.nomeMapa).append(":").append("\n");
        c = HeuristicasTSPGenericas.buscarCaminhoAleatoriamente(mp, 1000);
        sb.append("-> busca aleat�ria 1s: ").append(c.toStringDetalhado()).append("\n");
        c = HeuristicasTSPGenericas.buscarCaminhoAleatoriamente(mp, 600000);
        sb.append("-> busca aleat�ria 10min: ").append(c.toStringDetalhado()).append("\n");
        c = HeuristicasTSPGenericas.buscarCaminho2opt(mp, 1000);
        sb.append("-> busca 2opt 1s: ").append(c.toStringDetalhado()).append("\n");
        c = HeuristicasTSPGenericas.buscarCaminho2opt(mp, 600000);
        sb.append("-> busca 2opt 10min: ").append(c.toStringDetalhado()).append("\n");
        return sb.toString();
    }
}
