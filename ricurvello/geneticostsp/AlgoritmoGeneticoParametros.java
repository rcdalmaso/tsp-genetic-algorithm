package ricurvello.geneticostsp;

public class AlgoritmoGeneticoParametros implements Cloneable {
    // Valores padr�o dos par�metros:
    private double tamanhoPopulacaoRelativoAoTamanhoMapa = 2;
    private int tipoSelecaoNatural = 2;
    private int tipoCrossover = 2;
    private int tipoMutacao = 5;
    private double probMutacao = 0.2;
    private double mortalidadeSNAntesReproducao = 0.4;
    private double mortalidadeSNAposReproducao = 0.9;
    private long maximoGeracoes = 1000;
    private long maximoTempoMs = 600000;
    private int numeroTarefasEmParalelo = 8;

    /**
     * Interface Cloneable.
     */
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    
    public double getTamanhoPopulacaoRelativoAoTamanhoMapa() {
        return tamanhoPopulacaoRelativoAoTamanhoMapa;
    }

    /**
     * Define o valor para o parametro tamanhoPopulacaoRelativoAoTamanhoMapa.
     *
     * tamanhoPopulacaoRelativoAoTamanhoMapa = tamanho da popula��o em relacao ao tamanho do mapa.
     * 
     * (tamanhoPopulacaoRelativoAoTamanhoMapa > 0)
     */
    public void setTamanhoPopulacaoRelativoAoTamanhoMapa(
            double tamanhoPopulacaoRelativoAoTamanhoMapa) {
        if (tamanhoPopulacaoRelativoAoTamanhoMapa <= 0) {
            throw new IllegalArgumentException();
        }
        this.tamanhoPopulacaoRelativoAoTamanhoMapa =
                tamanhoPopulacaoRelativoAoTamanhoMapa;
    }

    public int getTipoSelecaoNatural() {
        return tipoSelecaoNatural;
    }

    /**
     * Define o valor para o par�metro tipoSelecaoNatural.
     *
     * tipoSelecaoNatural = 1 - "Elitista Uniforme"
     *                    = 2 - "Elitista Triangular"
     */
    public void setTipoSelecaoNatural(int tipoSelecaoNatural) {
        if (tipoSelecaoNatural < 1 || tipoSelecaoNatural > 2) {
            throw new IllegalArgumentException();
        }
        this.tipoSelecaoNatural = tipoSelecaoNatural;
    }

    public int getTipoCrossover() {
        return tipoCrossover;
    }

    /**
     * Define o valor para o par�metro tipoCrossover.
     *
     * tipoCrossover = 1 - "Greedy Crossover"
     *               = 2 - "Greedy Subtour Crossover (GSX)"
     */
    public void setTipoCrossover(int tipoCrossover) {
        if (tipoCrossover < 1 || tipoCrossover > 2) {
            throw new IllegalArgumentException();
        }
        this.tipoCrossover = tipoCrossover;
    }

    public int getTipoMutacao() {
        return tipoMutacao;
    }

    /**
     * Define o valor para o par�metro tipoMutacao.
     *
     * tipoMutacao = 1 - "Random Swap"
     *             = 2 - "Greedy Swap"
     *             = 3 - "2opt"
     *             = 4 - "Random Swap" + "2opt"
     *             = 5 - "Random Swap" + "2opt" (em 20% das gera��es) ou s�
     *                   "2opt" (em 80% gera��es)
     */
    public void setTipoMutacao(int tipoMutacao) {
        if (tipoMutacao < 1 || tipoMutacao > 5) {
            throw new IllegalArgumentException();
        }
        this.tipoMutacao = tipoMutacao;
    }

    public double getProbMutacao() {
        return probMutacao;
    }

    /**
     * Define o valor para o par�metro probMutacao.
     *
     * probMutacao = probabilidade de muta��o na geracao de um indiv�duo
     *
     * (1 >= probMutacao >= 0)
     */
    public void setProbMutacao(double d) {
        if (d < 0.0 || d > 1.0) {
            throw new IllegalArgumentException();
        }
        this.probMutacao = d;
    }

    public double getMortalidadeSNAntesReproducao() {
        return mortalidadeSNAntesReproducao;
    }

    /**
     * Define o valor para o par�metro mortalidadeSNAntesReproducao
     *
     * mortalidadeSNAntesReproducao = propor��o de indiv�duos eliminados na sele��o natural antes
     *                                da reprodu��o
     *
     * (mortalidadeSNAposReproducao >= mortalidadeSNAntesReproducao >= 0)
     */
    public void setMortalidadeSNAntesReproducao(double d) {
        if (d < 0 || d > mortalidadeSNAposReproducao) {
            throw new IllegalArgumentException();
        }
        this.mortalidadeSNAntesReproducao = d;
    }

    public double getMortalidadeSNAposReproducao() {
        return mortalidadeSNAposReproducao;
    }

    /**
     * Define o valor para o par�metro mortalidadeSNAposReproducao.
     *
     * mortalidadeSNAposReproducao = propor��o de indiv�duos eliminados na sele��o natural ap�s a
     *                               reprodu��o
     *
     * (1 >= mortalidadeSNAposReproducao >= mortalidadeSNAntesReproducao)
     */
    public void setMortalidadeSNAposReproducao(double d) {
        if (mortalidadeSNAntesReproducao > d || d > 1) {
            throw new IllegalArgumentException();
        }
        this.mortalidadeSNAposReproducao = d;
    }

    public long getMaximoGeracoes() {
        return maximoGeracoes;
    }

    /**
     * Define o valor para o par�metro maximoGeracoes.
     *
     * maximoGeracoes = n�mero m�ximo de gera��es a executar o algoritmo (crit�rio de parada)
     * 
     * (geracoes > 0)
     */
    public void setMaximoGeracoes(long maximoGeracoes) {
        if (maximoGeracoes <= 0) {
            throw new IllegalArgumentException();
        }
        this.maximoGeracoes = maximoGeracoes;
    }
    
    public void removerMaximoGeracoes() {
        this.maximoGeracoes = Long.MAX_VALUE;
    }
    
    public long getMaximoTempoMs() {
        return maximoTempoMs;
    }

    /**
     * Define o valor para o par�metro maximoTempoMs.
     *
     * maximoTempo = tempo m�ximo a executar o algoritmo em ms (crit�rio de parada)
     * 
     * (maximoTempo > 0)
     */
    public void setMaximoTempoMs(long maximoTempoMs) {
        if (maximoTempoMs <= 0) {
            throw new IllegalArgumentException();
        }
        this.maximoTempoMs = maximoTempoMs;
    }
    
    public void removerMaximoTempoMs() {
        this.maximoTempoMs = Long.MAX_VALUE;
    }
    
    public int getNumeroTarefasEmParalelo() {
        return numeroTarefasEmParalelo;
    }

    /**
     * Define o valor para o par�metro numeroTarefasEmParalelo.
     *
     * numeroTarefasEmParalelo = n�mero de execu��es em paralelo para as opera��es de crossover e
     *                           muta��o
     *
     * (numeroTarefasEmParalelo > 0)
     */
    public void setNumeroTarefasEmParalelo(int numeroTarefasEmParalelo) {
        if (numeroTarefasEmParalelo <= 0) {
            throw new IllegalArgumentException();
        }
        this.numeroTarefasEmParalelo = numeroTarefasEmParalelo;
    }

    public String getNomeSelecaoNatural() {
        if (this.tipoSelecaoNatural == 1) {
            return "\"Elitista Uniforme\"";
        } else {
            return "\"Elitista Triangular\"";
        }
    }

    public String getNomeCrossover() {
        if (this.tipoCrossover == 1) {
            return "\"Greedy Crossover\"";
        } else {
            return "\"Greedy Subtour Crossover (GSX)\"";
        }
    }

    public String getNomeMutacao() {
        if (this.tipoMutacao == 1) {
            return "\"Random Swap\"";
        } else if (this.tipoMutacao == 2) {
            return "\"Greedy Swap\"";
        } else if (this.tipoMutacao == 3) {
            return "\"2opt\"";
        } else if (this.tipoMutacao == 4) {
            return "\"Random Swap + 2opt\"";
        } else {
            return "\"Random Swap + 2opt (20% das gera��es) " +
                        "ou s� 2opt (80% gera��es)\"";
        }
    }

    public String toString() {
        String s = "tamanhoPopulacaoRelativoAoTamanhoMapa = "
                + tamanhoPopulacaoRelativoAoTamanhoMapa + "; selecao natural "
                + getNomeSelecaoNatural() + "; crossover " + getNomeCrossover() + "; mutacao "
                + getNomeMutacao() + "; probabilidade muta��o = " + probMutacao + "; mortalidade "
                + "sele��o natural antes reprodu��o = " + mortalidadeSNAntesReproducao + "; "
                + "mortalidade sele��o natural ap�os reproducao = "
                + mortalidadeSNAposReproducao;
        if (maximoGeracoes != Long.MAX_VALUE) {
            s = s + "; m�ximo gera��es = " + maximoGeracoes;
        }
        if (maximoTempoMs != Long.MAX_VALUE) {
            s = s + "; m�ximo tempo = " + maximoTempoMs + "ms";
        }
        s = s + "; paralelismo = " + numeroTarefasEmParalelo;
        return s;
    }

    public String toStringCompacto() {
        String s = "tamanhoPop" + tamanhoPopulacaoRelativoAoTamanhoMapa + "Sn" + tipoSelecaoNatural
                + "Cr" + tipoCrossover + "Mt" + tipoMutacao + "Pm" + probMutacao + "MortSn"
                + mortalidadeSNAntesReproducao + "e" + mortalidadeSNAposReproducao;
        if (maximoGeracoes != Long.MAX_VALUE) {
            s = s + "MaxGer" + maximoGeracoes;
        }
        if (maximoTempoMs != Long.MAX_VALUE) {
            s = s + "MaxT" + maximoTempoMs;
        }
        s = s + "Par" + numeroTarefasEmParalelo;
        return s;
    }
}
