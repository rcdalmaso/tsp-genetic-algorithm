package ricurvello.geneticostsp;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import ricurvello.geneticostsp.MapaPontos.Caminho;
import ricurvello.geneticostsp.MapaPontos.Ponto;
import ricurvello.geneticostsp.MapaPontos.PontoCartesiano;

public class UtilGraficos {
    private static final int altura = 600;
    private static final int largura = 800;

    public static void gravarPngEvolucaoAlgoritmoGenetico(AlgoritmoGenetico[] ags,
            String prefixoTitulo, File arqPng, boolean exibirTempos) throws IOException {
        if (ags == null || ags.length == 0) {
            throw new IllegalArgumentException();
        }
        MapaPontos mp = ags[0].m;
        long tempoMaximo = 0L;
        int numeroMaximoGeracoes = 0;
        List<XYSeries> dados = new ArrayList<XYSeries>();
        for (AlgoritmoGenetico ag : ags) {
            XYSeries serie = new XYSeries(ag.descricaoCompacta, false);
            List<Double> d = ag.distanciaOtimaPorGeracao;
            List<Long> l = ag.tempoGastoPorGeracao;
            for (int i = 1; i < d.size(); i++) {
                if (exibirTempos) {
                    serie.add(l.get(i), d.get(i));
                } else {
                    serie.add(i, d.get(i));
                }
                if (l.get(i) > tempoMaximo) {
                    tempoMaximo = l.get(i);
                }
            }
            if ((d.size() - 1) > numeroMaximoGeracoes) {
                numeroMaximoGeracoes = d.size() - 1;
            }
            dados.add(serie);
            if (ag.m != mp) {
                throw new IllegalArgumentException();
            }
        }
        // adicionando sequ�ncia com a dist�ncia �tima se ela for conhecida:
        double d = mp.distanciaOtimaTSPConhecida;
        if (d > 0.0) {
            XYSeries serie = new XYSeries("solu��o �tima", false);
            if (exibirTempos) {
                serie.add(0, d);
                serie.add(tempoMaximo, d);
            } else {
                serie.add(1, d);
                serie.add(numeroMaximoGeracoes, d);
            }
            dados.add(serie);
        }
        if (prefixoTitulo == null) {
            prefixoTitulo = "";
        }
        if (prefixoTitulo.length() > 0) {
            prefixoTitulo = prefixoTitulo + " - ";
        }
        if (exibirTempos) {
            salvarGraficoPng(prefixoTitulo + mp.nomeMapa, "tempo (ms)", "dist�ncia", true, dados,
                    arqPng, largura, altura);
        } else {
            salvarGraficoPng(prefixoTitulo + mp.nomeMapa, "# gera��o", "dist�ncia", true, dados,
                    arqPng, largura, altura);
        }
    }

    public static void gravarPngCaminhoNoMapa(Caminho c, String legenda, File arqPng)
            throws IOException {
        MapaPontos m = c.getMapaPontosAssociado();
        XYSeries serie = new XYSeries(legenda, false);
        for (int i = 0; i < m.numeroPontos; i++) {
            Ponto p = m.getPonto(c.ordem[i]);
            if (!(p instanceof PontoCartesiano)) {
                throw new IllegalArgumentException();
            }
            PontoCartesiano pc = (PontoCartesiano) p;
            serie.add(pc.x, pc.y);
        }
        PontoCartesiano p = (PontoCartesiano) m.getPonto(c.ordem[0]);
        serie.add(p.x, p.y);
        List<XYSeries> dados = new ArrayList<XYSeries>();
        dados.add(serie);
        salvarGraficoPng(m.nomeMapa, null, null, true, dados, arqPng, largura, altura);
    }

    public static void gravarPngRelacaoTempoTamanho(AlgoritmoGenetico[] ags, File arqPng)
            throws IOException {
        if (ags == null || ags.length == 0) {
            throw new IllegalArgumentException();
        }
        long numeroGeracoes = ags[0].p.getMaximoGeracoes();
        XYSeries serie = new XYSeries("serie", false);
        for (int i = 1; i < ags.length; i++) {
            serie.add(ags[i].m.numeroPontos, ags[i].tempoGastoSegundos);
            if (ags[i].p.getMaximoGeracoes() != numeroGeracoes) {
                throw new IllegalArgumentException();
            }
        }
        List<XYSeries> dados = new ArrayList<XYSeries>();
        dados.add(serie);
        salvarGraficoPng(null, "N�mero de pontos do mapa", "Tempo gasto nas " + numeroGeracoes
                + " gera��es", false, dados, arqPng, largura, altura);
    }

    public static void salvarGraficoPng(String tituloGrafico, String nomeEixoX, String nomeEixoY,
            boolean incluirLegenda, List<XYSeries> dados, File f, int largura, int altura)
            throws IOException {
        if (dados == null || dados.size() < 1) {
            throw new IllegalArgumentException();
        }
        XYSeriesCollection dataset = new XYSeriesCollection();
        double minimoX = dados.get(0).getMinX();
        double minimoY = dados.get(0).getMinY();
        double maximoX = dados.get(0).getMaxX();
        double maximoY = dados.get(0).getMaxY();
        dataset.addSeries(dados.get(0));
        for (int i = 1; i < dados.size(); i++) {
            if (dados.get(i).getMinX() < minimoX) {
                minimoX = dados.get(i).getMinX();
            }
            if (dados.get(i).getMinY() < minimoY) {
                minimoY = dados.get(i).getMinY();
            }
            if (dados.get(i).getMaxX() > maximoX) {
                maximoX = dados.get(i).getMaxX();
            }
            if (dados.get(i).getMaxY() > maximoY) {
                maximoY = dados.get(i).getMaxY();
            }
            dataset.addSeries(dados.get(i));
        }
        double intervaloX = maximoX - minimoX;
        double intervaloY = maximoY - minimoY;
        if (intervaloX == 0.0) {
            intervaloX = 1.0;
        }
        if (intervaloY == 0.0) {
            intervaloY = 1.0;
        }
        minimoX = minimoX - 0.01 * intervaloX;
        maximoX = maximoX + 0.01 * intervaloX;
        minimoY = minimoY - 0.01 * intervaloY;
        maximoY = maximoY + 0.01 * intervaloY;
        JFreeChart jfc = ChartFactory.createXYLineChart(tituloGrafico, nomeEixoX, nomeEixoY,
                dataset, PlotOrientation.VERTICAL, incluirLegenda, false, false);
        jfc.getXYPlot().getRangeAxis().setRange(minimoY, maximoY);
        jfc.getXYPlot().getDomainAxis().setRange(minimoX, maximoX);
        jfc.setBackgroundPaint(Color.white);
        ChartUtilities.saveChartAsPNG(f, jfc, largura, altura);
    }
}
