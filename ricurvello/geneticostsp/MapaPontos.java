package ricurvello.geneticostsp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.ThreadLocalRandom;

public class MapaPontos {
    public final String nomeMapa;
    public final int numeroPontos;
    public final double distanciaOtimaTSPConhecida;
    private final List<Ponto> pontos;
    private final double[][] matrizDistancias;

    /**
     * Construtor
     */
    private MapaPontos(String nomeMapa, List<Ponto> pontos, double[][] matrizDistancias,
            double distanciaOtimaTSPConhecida) {
        this.nomeMapa = nomeMapa;
        this.numeroPontos = pontos.size();
        this.pontos = pontos;
        this.matrizDistancias = matrizDistancias;
        this.distanciaOtimaTSPConhecida = distanciaOtimaTSPConhecida;
    }

    /**
     * M�todo que carrega um mapa de pontos cartesianos de um arquivo.
     * 
     * Observa��o: este m�todo adota algumas op��es padr�o, para outros casos de uso, utilizar o
     * m�todo "carregarMapaCartesiano".
     */
    public static MapaPontos carregarMapa(File dirEntradaMapas, String nomeArquivoMapa,
            double valorOtimoConhecido) throws Exception {
        int i = nomeArquivoMapa.indexOf(".tsp");
        String nomeMapa = null;
        if (i == -1) {
            nomeMapa = nomeArquivoMapa;
        } else {
            nomeMapa = nomeArquivoMapa.substring(0, i);
        }
        MapaPontos mp = MapaPontos.carregarMapaCartesiano(dirEntradaMapas, nomeArquivoMapa,
                nomeMapa, true, valorOtimoConhecido);
        System.out.println(mp);
        return mp;
    }

    /**
     * M�todo que carrega um mapa de pontos cartesianos de um arquivo.
     * 
     * Cada linha do arquivo deve conter 3 informa��es separadas por espa�os, sendo a primeira o
     * identificador do ponto e as duas seguintes os valores das coordenadas x e y.
     */
    public static MapaPontos carregarMapaCartesiano(File diretorio, String arquivo,
            String nomeMapa, boolean ignorarLinhasInvalidas, double distanciaOtimaTSPConhecida)
            throws IOException {
        File f = new File(diretorio, arquivo);
        List<Ponto> pontos = new ArrayList<Ponto>();

        // 1. leitura do arquivo:
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(f));
            String linha = br.readLine();
            while (linha != null) {
                StringTokenizer st = new StringTokenizer(linha);
                try {
                    int numeroTokens = st.countTokens();
                    if (numeroTokens == 3) {
                        String s1 = st.nextToken();
                        Double d2 = Double.parseDouble(st.nextToken());
                        Double d3 = Double.parseDouble(st.nextToken());
                        pontos.add(new PontoCartesiano(s1, d2, d3));
                    } else {
                        if (!ignorarLinhasInvalidas) {
                            throw new IOException("Conteudo invalido em " + arquivo
                                    + " - linha com numero de campos " + "diferente de 3.");
                        }
                    }
                } catch (NumberFormatException nfe) {
                    if (!ignorarLinhasInvalidas) {
                        throw new IOException("Conteudo invalido em " + arquivo
                                + " - formato invalido, esperado valor numerico.");
                    }
                }
                linha = br.readLine();
            }
        } finally {
            if (br != null) {
                br.close();
            }
        }
        int n = pontos.size();
        if (pontos.size() < 2) {
            throw new IOException("Conteudo invalido em " + arquivo
                    + " - menos de 2 pontos definidos.");
        }

        // 2. construcao da matriz de distancias:
        double[][] matrizDistancias = new double[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < i; j++) {
                PontoCartesiano pontoI = (PontoCartesiano) pontos.get(i);
                PontoCartesiano pontoJ = (PontoCartesiano) pontos.get(j);
                matrizDistancias[i][j] = Math.pow(
                        Math.pow(pontoI.x - pontoJ.x, 2) + Math.pow(pontoI.y - pontoJ.y, 2), 0.5);
                matrizDistancias[j][i] = matrizDistancias[i][j];
            }
            matrizDistancias[i][i] = 0.0;
        }

        return new MapaPontos(nomeMapa, pontos, matrizDistancias, distanciaOtimaTSPConhecida);
    }

    /**
     * Classe interna que representa um ponto no mapa.
     */
    public static class Ponto {
        public final String id;

        private Ponto(String id) {
            this.id = id;
        }

        public String toString() {
            return this.id;
        }
    }

    /**
     * Classe interna que representa um ponto cartesiano no mapa.
     */
    public static class PontoCartesiano extends Ponto {
        public final double x;
        public final double y;

        private PontoCartesiano(String id, double x, double y) {
            super(id);
            this.x = x;
            this.y = y;
        }

        public String toString() {
            return "\"" + super.toString() + "\": " + this.x + "; " + this.y;
        }
    }

    /**
     * M�todo para obter um ponto do mapa (0 <= i < numeroPontos)
     */
    public Ponto getPonto(int i) {
        return this.pontos.get(i);
    }

    /**
     * M�todo para obter a distancia entre dois pontos do mapa
     */
    public double getDistancia(int i, int j) {
        return this.matrizDistancias[i][j];
    }

    /**
     * Classe interna que representa um caminho no mapa. O caminho � circular, ou seja, termina no
     * mesmo ponto onde se iniciou.
     */

    public class Caminho {
        // Array que representa o ordem dos pontos do mapa:
        public final int[] ordem;

        private Caminho() {
            this.ordem = new int[numeroPontos];
        }

        private Caminho(int[] ordemPontos) {
            this.ordem = ordemPontos;
        }

        public String toString() {
            StringBuffer sb = new StringBuffer("Caminho: ");
            for (int i = 0; i < this.ordem.length; i++) {
                sb.append("(");
                sb.append(getPonto(this.ordem[i]).id);
                sb.append(")");
            }
            sb.append("(");
            sb.append(getPonto(0).id);
            sb.append(")");
            return sb.toString();
        }

        public String toStringDetalhado() {
            StringBuffer sb = new StringBuffer(nomeMapa);
            sb.append(": dist�ncia ");
            sb.append(getDistanciaAssociada(this.ordem));
            sb.append(". ");
            sb.append(this.toString());
            return sb.toString();
        }

        public int hashCode() {
            return Arrays.hashCode(ordem);
        }

        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            if (!Arrays.equals(ordem, ((Caminho) obj).ordem))
                return false;
            return true;
        }

        public MapaPontos getMapaPontosAssociado() {
            return MapaPontos.this;
        }
    }

    /**
     * M�todo que gera um novo caminho aleat�rio no mapa sem passar duas vezes pelo mesmo ponto,
     * iniciando pelo primeiro ponto e percorrendo todo o mapa.
     */
    public Caminho gerarNovoCaminhoAleatorio() {
        Caminho c = new Caminho();
        ArrayList<Integer> aIncluir = new ArrayList<Integer>(this.numeroPontos);
        for (int i = 1; i < this.numeroPontos; i++) {
            aIncluir.add(i);
        }
        c.ordem[0] = 0;
        int posicaoSorteada;
        int valorSorteado;
        int tamanho;
        for (int i = 1; i < this.numeroPontos; i++) {
            tamanho = aIncluir.size();
            posicaoSorteada = ThreadLocalRandom.current().nextInt(tamanho);
            valorSorteado = aIncluir.remove(posicaoSorteada);
            c.ordem[i] = valorSorteado;
        }
        return c;
    }

    /**
     * M�todo que gera um novo caminho vazio no mapa com comprimento igual ao n�mero de pontos do
     * mapa.
     */
    public Caminho gerarNovoCaminhoVazio() {
        return new Caminho();
    }

    /**
     * M�todo que gera um novo caminho com a ordem de pontos especificada.
     */
    public Caminho gerarNovoCaminho(int[] ordemPontos) {
        if (ordemPontos == null) {
            throw new IllegalArgumentException();
        }
        return new Caminho(ordemPontos);
    }

    /**
     * M�todo para obter a distancia associada a uma sequencia circular de pontos no mapa.
     */
    public double getDistanciaAssociada(int[] ordemPontos) {
        double d = 0;
        int n = ordemPontos.length;
        for (int i = 1; i < n; i++) {
            d += this.matrizDistancias[ordemPontos[i - 1]][ordemPontos[i]];
        }
        d += this.matrizDistancias[ordemPontos[n - 1]][ordemPontos[0]];
        return d;
    }

    /**
     * M�todo para obter uma representacao do mapa em uma String.
     */
    public String toString() {
        StringBuffer sb = new StringBuffer("Mapa ");
        sb.append(this.nomeMapa);
        sb.append(":");
        for (int i = 0; i < this.numeroPontos; i++) {
            sb.append("\n(").append(i + 1).append(") ");
            sb.append(this.pontos.get(i).toString());
        }
        return sb.toString();
    }
}
