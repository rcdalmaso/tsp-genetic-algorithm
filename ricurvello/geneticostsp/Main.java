package ricurvello.geneticostsp;

import java.io.File;
import java.io.PrintStream;
//import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            throw new Exception("Informe o diret�rio base contendo \"entrada-mapas\" e \"saida\"");
        }

        String timeStamp = UtilFormatos.formatarDataSemBarras(new Date());
        File dirMapas = new File(args[0] + "entrada-mapas");
        File dirLogs = new File(args[0] + "saida", timeStamp + "-logs");
        File dirRelatorios = new File(args[0] + "saida", timeStamp + "-info");

        if (dirLogs.exists() || dirRelatorios.exists()) {
            throw new Exception("Diret�rio de sa�da j� existente.");
        }
        if (!dirLogs.mkdirs() || !dirRelatorios.mkdirs()) {
            throw new Exception("N�o foi poss�vel criar diret�rio de sa�da.");
        }

        AlgoritmoGeneticoComparacoes x = new AlgoritmoGeneticoComparacoes(dirLogs, dirRelatorios);
        String txt;
        int comparacoes;

        System.out.println("****************************************");

        System.out.println("Carregamento dos mapas:");
        MapaPontos mapaBurma14 = MapaPontos.carregarMapa(dirMapas, "burma14.tsp", 30.878503892588);
        MapaPontos mapaCh150 = MapaPontos.carregarMapa(dirMapas, "ch150.tsp", 6528);
        MapaPontos mapaA280 = MapaPontos.carregarMapa(dirMapas, "a280.tsp", 2579);
        MapaPontos mapaPcb442 = MapaPontos.carregarMapa(dirMapas, "pcb442.tsp", 50778);
        MapaPontos mapaU574 = MapaPontos.carregarMapa(dirMapas, "u574.tsp", 36905);
        MapaPontos mapaPr1002 = MapaPontos.carregarMapa(dirMapas, "pr1002.tsp", 259045);
        MapaPontos mapaPcb1173 = MapaPontos.carregarMapa(dirMapas, "pcb1173.tsp", 56892);
        MapaPontos mapaPr2392 = MapaPontos.carregarMapa(dirMapas, "pr2392.tsp", 378032);
        MapaPontos mapaPcb3038 = MapaPontos.carregarMapa(dirMapas, "pcb3038.tsp", 137694);
        MapaPontos mapaFnl4461 = MapaPontos.carregarMapa(dirMapas, "fnl4461.tsp", 182566);
        MapaPontos mapaRl5915 = MapaPontos.carregarMapa(dirMapas, "rl5915.tsp", 565530);
        MapaPontos[] todosOsMapas = new MapaPontos[] {
                mapaBurma14,
                mapaCh150,
                mapaA280,
                mapaPcb442,
                mapaU574,
                mapaPr1002,
                mapaPcb1173,
                mapaPr2392,
                mapaPcb3038,
                mapaFnl4461,
                mapaRl5915,
        };

        System.out.println("****************************************");
        System.out.println("Etapa 1 - Enumera��o completa - burma14");
        System.out.println();

        AlgoritmoEnumeracaoCompleta aec = new AlgoritmoEnumeracaoCompleta(mapaBurma14, dirLogs);
        aec.gerarRelatorio(dirRelatorios, "Etapa1");
        aec.gerarImagemMelhorCaminhoNoMapa(dirRelatorios, "Etapa1");

        System.out.println("****************************************");
        System.out.println("Etapa 2 - Busca heur�stica");
        System.out.println();

        PrintStream ps = new PrintStream(new File(dirRelatorios, "Etapa2.txt"));
        for (MapaPontos mp : todosOsMapas) {
            txt = HeuristicasTSPGenericas.gerarRelatorioComparativoBuscas(mp);
            ps.println(txt);
            System.out.println(txt);
        }
        ps.close();

        System.out.println("****************************************");
        System.out.print("Etapa 3 - ");
        txt = "Compara��o paralelismo em 1000 gera��es";
        System.out.println(txt);
        System.out.println();

        comparacoes = 3;
        int[] paralelismos = new int[] { 1, 2, 4, 8, 16 };

        x.paralelismo1000Geracoes(mapaBurma14, paralelismos, comparacoes, "Etapa3", txt);
        x.paralelismo1000Geracoes(mapaA280, paralelismos, comparacoes, "Etapa3", txt);
        x.paralelismo1000Geracoes(mapaPcb442, paralelismos, comparacoes, "Etapa3", txt);
        x.paralelismo1000Geracoes(mapaU574, paralelismos, comparacoes, "Etapa3", txt);

        System.out.println("****************************************");
        System.out.print("Etapa 4 - ");
        txt = "Compara��o tamanho da popula��o em 10 minutos";
        System.out.println(txt);
        System.out.println();

        comparacoes = 5;
        double[] tamanhosPopulacao = new double[] { 0.5, 1, 1.5, 2, 3, 5, 10 };

        x.tamanhoPopulacao10minutos(mapaPcb442, tamanhosPopulacao, comparacoes, "Etapa4", txt);
        x.tamanhoPopulacao10minutos(mapaU574, tamanhosPopulacao, comparacoes, "Etapa4", txt);
        x.tamanhoPopulacao10minutos(mapaPr1002, tamanhosPopulacao, comparacoes, "Etapa4", txt);
        x.tamanhoPopulacao10minutos(mapaPcb1173, tamanhosPopulacao, comparacoes, "Etapa4", txt);

        System.out.println("****************************************");
        System.out.print("Etapa 5a - ");
        txt = "Compara��o tipo e probabilidade muta��o em 10 minutos";
        System.out.println(txt);
        System.out.println();

        comparacoes = 1;
        int[] tiposMutacao = new int[] { 1, 2, 3 };
        double[] probMutacao = new double[] { 0.0, 0.5 };

        x.mutacao10minutos(mapaCh150, tiposMutacao, probMutacao, comparacoes, "Etapa5a", txt);
        x.mutacao10minutos(mapaPcb442, tiposMutacao, probMutacao, comparacoes, "Etapa5a", txt);

        System.out.println("****************************************");
        System.out.print("Etapa 5b - ");
        txt = "Compara��o tipo e probabilidade muta��o em 10 minutos";
        System.out.println(txt);
        System.out.println();

        comparacoes = 5;
        tiposMutacao = new int[] { 3, 4, 5 };
        probMutacao = new double[] { 0.2, 0.5, 0.8, 1.0 };

        x.mutacao10minutos(mapaPcb442, tiposMutacao, probMutacao, comparacoes, "Etapa5b", txt);
        x.mutacao10minutos(mapaU574, tiposMutacao, probMutacao, comparacoes, "Etapa5b", txt);

        System.out.println("****************************************");
        System.out.print("Etapa 6 - ");
        txt = "Compara��o mortalidades sele��o natural em 10 minutos";
        System.out.println(txt);
        System.out.println();

        comparacoes = 5;
        double[][] mortalidadesSN = new double[][] {
                { 0.2, 0.5 },
                { 0.2, 0.9 },
                { 0.4, 0.9 },
                { 0.6, 0.9 },
                { 0.8, 0.9 },
                { 0.9, 0.98 },
        };

        x.mortalidadesSN10minutos(mapaPcb442, mortalidadesSN, comparacoes, "Etapa6", txt);
        x.mortalidadesSN10minutos(mapaU574, mortalidadesSN, comparacoes, "Etapa6", txt);

        System.out.println("****************************************");
        System.out.print("Etapa 7 - ");
        txt = "Compara��o tipos sele��o natural e crossover em 10 minutos";
        System.out.println(txt);
        System.out.println();

        comparacoes = 5;
        int[][] tiposSNCrossover = new int[][] {
                { 1, 1 }, { 1, 2 },
                { 2, 1 }, { 2, 2 },
        };

        x.tipoSNCrossover10minutos(mapaPcb442, tiposSNCrossover, comparacoes, "Etapa7", txt);
        x.tipoSNCrossover10minutos(mapaU574, tiposSNCrossover, comparacoes, "Etapa7", txt);
        x.tipoSNCrossover10minutos(mapaPr1002, tiposSNCrossover, comparacoes, "Etapa7", txt);
        x.tipoSNCrossover10minutos(mapaPcb1173, tiposSNCrossover, comparacoes, "Etapa7", txt);

        System.out.println("****************************************");
        System.out.println("Etapas 8/9 - Aplica��o algoritmo em 5000 gera��es");
        System.out.println();
        List<AlgoritmoGenetico> lag = new ArrayList<AlgoritmoGenetico>();
        for (MapaPontos mp : todosOsMapas) {
            lag.add(x.aplicacao5000Geracoes(mp, "Etapa8", "Aplica��o algoritmo em 5000 gera��es"));
        }
        AlgoritmoGeneticoRelatorios.graficoTempoExecucaoTamanhoMapa(dirRelatorios, "Etapa9", lag);

        System.out.println("****************************************");
        System.out.println("<fim do processamento>");
    }
}
